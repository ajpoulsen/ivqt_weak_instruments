
* A simple bivariate model
clear

set obs 10000

gen x = runiform() * 50

gen eps = rnormal(0,100)

gen y = 1 + 5*x + eps

reg y x

forvalues X = .1(.1).9 {
	qreg y x ,q(`X')
	}

* Now a model with endogeneity

clear

set obs 1000000

gen eps_z = rnormal(0,100)
gen eps_d = rnormal(0,100)
gen eps_y = rnormal(0,100)

gen z = 1* (eps_z > 0)

gen d = 1 * (1*eps_z+eps_d + eps_y > 0)

gen y = 1 + 5*d + eps_y

reg y d
ivreg y (d=z)

* First stage
reg d z
reg y z


* Now a model with a weak instrument

clear

set obs 10000

gen eps_z = rnormal(0,10)
gen eps_d = rnormal(0,100)
gen eps_y = rnormal(0,100)

gen z = 1* (eps_z > 0)

gen d = 1 * (.3*eps_z + eps_d + eps_y > 0)

gen y = 1 + 50*d + eps_y

reg y d
ivreg y (d=z)

* First stage
reg d z


reg y z



* Testing the one made in python

import delim y d z using data_2_28_strong.csv, clear

reg d z
ivreg y (d=z)


ivqreg  y (d=z),q(.5)
mat h = e(h)
mat list h

bootstrap, reps(10) :ivqreg  y (d=z)


mat boots = J(100,2,.)
forval X = 1/100{
	import delim y d z using data_2_28_strong.csv, clear
	bsample
	qui ivqreg  y (d=z)
	mat es = e(b)
	mat boots[`X',1] = es[1,1]
	mat boots[`X',2] = es[1,2]
}


mata sqrt(variance(st_matrix("boots")))



** Now testing the AAI estimator
ivqte y (d=z), aai q(.5)








** Testing CH estimator
mat MC = J(100,4,.)
forval X = 1/100{
	clear
	set obs 10000
	
	gen Y_0 = exp(rnormal())
	gen Y_1 = 100*Y_0
	gen eps_z = rnormal()*10
	gen Z = 1*(eps_z <.5)
	
	qui summarize Y_0, detail
	sca def  q5 =  r(p50)
	sca def  q25 =  r(p25)
	sca def  q75 =  r(p75)
	
	qui summarize Y_1, detail
	sca def  qT5 = r(p50)
	
	gen NT = 1*(Y_0<q25)
	gen AT = 1*((Y_0>q25) & (Y_0<q5))
	gen  C = 1*(Y_0>q5)
	
	gen D = 0
	
	replace D = 0     if NT == 1
	replace D = 1     if AT == 1
	replace D = Z     if  C == 1
	
	gen Y = Y_0*(1-D) + Y_1*D
	*reg Y D
	*reg D Z
	qui ivqreg  Y (D=Z)
	mat es = e(b)
	mat MC[`X',1] = es[1,1]
	mat MC[`X',2] = es[1,2]
	mat MC[`X',3] = qT5 - q5
	mat MC[`X',4] = (MC[`X',1] - MC[`X',3])^2
	display "Done with ", `X' 
}

mata mean(st_matrix("MC"))




mata sqrt(variance(st_matrix("boots")))








* Trying for stronger instrument

** Testing CH estimator
mat MC = J(100,2,.)
mat truth = J(100,1,.)
forval X = 1/100{
	clear
	set obs 10000
	
	gen Y_0 = exp(rnormal())
	gen Y_1 = 100*Y_0
	gen eps_z = rnormal()*10
	gen Z = 1*(eps_z <.5)
	
	qui summarize Y_0, detail
	sca def  q5 =  r(p50)
	sca def  q05 =  r(p5)
	sca def  q95 =  r(p95)
	
	qui summarize Y_1, detail
	sca def  qT5 = r(p50)
	
	gen NT = 1*(Y_0<q05)
	gen DE = 1*((Y_0>q05) & (Y_0<q5))
	gen  C = 1*((Y_0>q5) & (Y_0<q95))
	gen AT = 1*(Y_0>q95)
	
	gen D = 0
	
	replace D = 1 - Z if DE == 1
	replace D = 0     if NT == 1
	replace D = 1     if AT == 1
	replace D = Z     if  C == 1
	
	gen Y = Y_0*(1-D) + Y_1*D
	reg Y D
	reg D Z
	ivqreg  Y (D=Z)
	mat es = e(b)
	mat MC[`X',1] = es[1,1]
	mat MC[`X',2] = es[1,2]
}



* Testing standard errors of AAI
set matsize 2000

mat mc = J(2000,1,.)
mat mc_var = J(2000,1,.)
mat mc_se = J(2000,1,.)
mat cov_l = J(2000,1,.)
mat cov_r = J(2000,1,.)
mat cov = J(2000,1,.)

forval X = 1/2000{

clear

qui set obs 1000

gen eps_z = rnormal(0,10)
gen eps_d = rnormal(0,10)
gen eps_y = rnormal(0,10)

gen z = 1* (eps_z > 0)
gen d = 1 * (15*z + eps_d + eps_y > 0)
gen y = 1 + 100*d + eps_y


qui ivqte y (d=z), q(.5) v
mat est_ = e(b)
mat var_ = e(V)
mat mc[`X',1] = est_[1,1]
mat mc_var[`X',1] = var_[1,1]

}

forval X = 1/2000{
mat mc_se[`X',1] = mc_var[`X',1]^(.5)

mat cov_r[`X',1] = (mc[`X',1] < 100 + 1.96*mc_se[`X',1])
mat cov_l[`X',1]  = (mc[`X',1] > 100 - 1.96*mc_se[`X',1])
mat cov[`X',1] = cov_r[`X',1] * cov_l[`X',1]

}
mata mean(st_matrix("cov"))


*********************************
* Testing standard errors of CH


mat mc = J(2000,1,.)
mat mc_var = J(2000,1,.)
mat mc_se = J(2000,1,.)
mat cov_l = J(2000,1,.)
mat cov_r = J(2000,1,.)
mat cov = J(2000,1,.)

forval X = 1/2000{

clear

qui set obs 1000

gen eps_z = rnormal(0,10)
gen eps_d = rnormal(0,10)
gen eps_y = rnormal(0,10)

gen z = 1* (eps_z > 0)
gen d = 1 * (15*z + eps_d + eps_y > 0)
gen y = 1 + 100*d + eps_y


qui ivqreg y (d=z), q(.5) g(80)
mat est_ = e(b)
mat var_ = e(V)
mat mc[`X',1] = est_[1,1]
mat mc_var[`X',1] = var_[1,1]

}

forval X = 1/2000{
mat mc_se[`X',1] = mc_var[`X',1]^(.5)

mat cov_r = (mc[`X',1] < 100 + 1.96*mc_se[`X',1])
mat cov_l  = (mc[`X',1] > 100 - 1.96*mc_se[`X',1])
mat cov[`X',1] = cov_r[`X',1] * cov_l[`X',1]

}
mata mean(st_matrix("cov"))
