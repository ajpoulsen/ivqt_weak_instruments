
import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model
import statsmodels.api as smm
from matplotlib import pyplot as plt
from numpy import linalg
import scipy.optimize as spicy
import os 

# /Users/poulsena/Documents/ivqt_weak_instruments

# Making Data
n = 1000

eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
strength = .9
z = 1*(eps_z > 0).reshape(n,1)
d = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
y = (1 + 100*d + eps_y).reshape(n,1)

data = np.array([y,d,z])
data = data[:,:,0].T


# First Stage
FS = smm.OLS(d, exog=smm.add_constant(z))
fs_fit = FS.fit()
print fs_fit.params
print fs_fit.bse

# Make sure the FS is significant
print fs_fit.params[1]/fs_fit.bse[1]

# Reduced form
RF = smm.OLS(y, exog=smm.add_constant(z))
RF_fit = RF.fit()
print RF_fit.params
print RF_fit.bse

# Here is the IV coeff.
print "Indirect least squares gives: ",  RF_fit.params[1]/fs_fit.params[1]




def K(x):
    """ Standard Normal Kernel"""
    return np.exp((-1)*x**2/2)/(2*np.pi)**.5

################################################################################################
####  Quantile Instrumental Variables Regression (Chernozhukov & Hansen)
def QTreg_solve(betaguess, tau, y, d):
	d = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	u = y - (np.dot(d,betaguess)).reshape(n,1)
	# here is the tilted absolute value function
	u_pos = np.where(u>0)[0]
	u_neg = np.where(u<=0)[0]
	rho_pos = u[u_pos]*tau 
	rho_neg = u[u_neg]*(1-tau)
	return rho_pos.sum() + np.abs(rho_neg.sum())


def IVQT_CH(betaguess, tau, y, d, z, gridlen,graphs = False,var = False):
    """This function is based off of Chernozhukov & Hansen (2003)."""

    alpha = np.linspace(50-.25*y.var()**.5,50+.25*y.var()**.5,gridlen) # 50 b/c that is the truth
    V = y - d*alpha # creates a matrix (n x gridlen)
    #regressor = np.concatenate((d,z),axis=1)
    betas = np.zeros((gridlen,z.shape[1]+1))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), z)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell')
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,0]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)

    # Estimating Variance/ Confidence Intervals
    if var == True:
        k = 2
        D_cons = np.concatenate((np.ones(n).reshape(n,1),d),axis=1)
        eps_hat = y - np.dot(D_cons,rightcoeffs[1:]).reshape(n,1)
        psi = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
        s = (tau - 1*(eps_hat < 0))* psi
        S = np.dot(s.T,s)*(1.0/n)
        S = np.zeros((k,k))
        for i in range(n):
            s_i = s[i].reshape(k,1)
            S += np.dot(s_i,s_i.T)
        S *= (1.0/n)

        sigma_hat = (eps_hat.var())**.5
        h = (4*sigma_hat**5/(3*n))**(1.0/5)

        DX = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
        J = np.zeros((k,k))
        for i in range(n):
            psi_i = psi[i].reshape(k,1)
            DX_i = DX[i].reshape(1,k)
            J += np.dot(psi_i,DX_i)* K(eps_hat[i]/h)*(1/h)
        J *= (1.0/n)

        omega = (1.0/n)*np.dot(np.dot(np.linalg.inv(J),S),np.linalg.inv(J.T))
        std_err =  np.diagonal(omega**.5)
        c = 1.96 # from the T-table
        # Confidence Intervals
        upper = rightalpha + c*std_err[0]
        lower = rightalpha - c*std_err[0]
    #plot this function to make sure our min is an interior point
    if graphs == True:
        plt.plot(alpha,abs(betas[:,0]))
        plt.plot(rightalpha,abs(betas[rightalpha_i][0]),'ro')
        plt.axvspan(lower, upper, facecolor='#00cccc')
        plt.ylabel('Coefficient of Z')
        plt.xlabel('Coefficient on D')
        plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
        plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
        plt.cla()
        #plt.show()
    print "Done with quantile ", tau
    return rightcoeffs, std_err, success.sum() == gridlen,J,S


os.chdir("/Users/poulsena/Documents/ivqt_weak_instruments/C_H/biggrid")

"""
betaguess = np.array([1,1])
gridlen = 100
quantiles = np.linspace(1,9,num=9)
nq = len(quantiles)

k = len(betaguess)
coeffs_CH = np.zeros((nq,k+1)) #(z_minned,constant,d )
std_errs_CH = np.zeros((nq,k))
successIVQ = np.zeros((nq,1))

for i in range(nq):
    tau = quantiles[i]*.1
    ivqreg = IVQT_CH(betaguess, tau, y, d, z, gridlen)
    coeffs_CH[i] = ivqreg[0]
    std_errs_CH[i] = ivqreg[1]
    successIVQ[i] = ivqreg[2]

print successIVQ.sum()
print "(z, constant, d )"
print coeffs_CH
print "(d, constant)"
print std_errs_CH

"""

"""
# an equivalent way of getting S
sss = np.zeros((k,k))
for i in range(n):
    sss += np.dot(psi[i].reshape((k,1)),psi[i].reshape((1,k)))

sss *= tau*(1-tau)/n
"""


################################################################################################
# Abadie, Angrist, Imbens (2002) approach to quantile instrumental variables
os.chdir("..")
import statsmodels.discrete.discrete_model as sm

def AAIsolve(betaguess, tau, y, d, z, Wplus):
    D = (d.copy()).reshape(n,1)
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = Wplus[u_pos]
    Wplus_neg = Wplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def IVQT_AAI(betaguess, tau, y, d, z):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    D = d.copy()
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    # This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)
    #prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*(D_cons[:,0].reshape(n,1))),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)

    D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    W = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    Wplus = np.array([np.zeros(len(W)),W]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    print "Percent of negative weights: ", 1.0*(W < 0).sum() / len(W)
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z, Wplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')

    # Standard Errors
    k = 2
    # Get rid of 0 weights
    pos = np.where(Wplus>0)[0]
    n_ = len(pos)
    y_ = y[pos]
    D_ = D[pos]
    z_ = z[pos]
    Wplus_ = Wplus[pos]
    prZ_ = prZ[pos]

    D_cons = np.concatenate((D_.reshape(n_,1),np.ones(n_).reshape(n_,1)),axis=1)
    eps_hat = y_ - np.dot(D_cons,betaIVQT.x).reshape(n_,1)

    sigma_hat = (eps_hat.var())**.5
    h = (4*sigma_hat**5/(3*n_))**(1.0/5)

    J = np.zeros((k,k))
    for i in range(n_):
        W_i = D_cons[i].reshape(k,1)
        J += np.dot(W_i,W_i.T)* K(eps_hat[i]/h)*(1/h) * Wplus_[i]
    J *= (1.0/n_)

    Sigma = np.zeros((k,k))
    for i in range(n_):
        W_i = D_cons[i].reshape(k,1)
        H_i = (tau - 1*(eps_hat[i]<0))* W_i * ( (1-D_[i])*z_[i]/prZ_[i]**2 - (1-z_[i])*D_[i]/(1-prZ_[i])**2 )
        psi_i = Wplus_[i]* (tau - 1*(eps_hat[i]<0))* W_i + H_i * (z_[i] - prZ_[i])
        Sigma += np.dot(psi_i,psi_i.T) 
    Sigma *= (1.0/n_)
    omega = (1.0/n_)*np.dot(np.dot(np.linalg.inv(J),Sigma),np.linalg.inv(J.T))

    std_err =  np.diagonal(omega**.5)

    return betaIVQT, std_err


"""

betaguess = np.array([100,1])
# Graph of each coefficient by IV quantile
nq = 9
quantiles = np.linspace(.1,.9,nq)
coeffs_AAI = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

for i in range(nq):
    tau = quantiles[i]
    betaIVQT,sterr = IVQT_AAI(betaguess, tau, y, d, z)
    coeffs_AAI[i] = betaIVQT.x
    success[i] = betaIVQT.success
    print "Done with quantile ", tau

print success
print "(d, cons)"
print coeffs_AAI
"""


################################################################################################
# The Inverse CDF approach to quantile instrumental variables (Frandsen)

def CDF(yguess, X, XX, Y, D,delta_FS,tau):
    W = (Y <= yguess)*D

    XW = np.dot(X.T,W)
    b_fs = np.linalg.solve(XX,XW)
    delta_RF = b_fs[0,0]

    beta_2sls = delta_RF / delta_FS

    diff = beta_2sls - tau
    return diff


def point_est_CDF(tau, y, D, z):
    #********* This finds Y(1) ************
    # First stage regression
    X = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
    XX = np.dot(X.T,X)
    XD = np.dot(X.T,D)
    b_fs = np.linalg.solve(XX,XD)
    delta_FS = b_fs[0,0]
    lo = y.min()
    up = y.max()

    y_1 = spicy.brentq(CDF,lo,up,args=(X, XX, Y, D, delta_FS, tau))
    # Maybe experiment withy changing tolerance in this function

    #********* This finds Y(0) ************
    D_0 = 1 - D
    # First stage regression
    XD_0 = np.dot(X.T,D_0)
    b_fs = np.linalg.solve(XX,XD_0)
    delta_FS = b_fs[0,0]

    y_0 = spicy.brentq(CDF,lo,up,args=(X, XX, y, D_0, delta_FS, tau))

    treatment = y_1-y_0
    return treatment



# Lets do some bootstraps
bootsnum = 100
bootstraps = np.zeros(bootsnum)
data = np.concatenate((y,D,z),axis=1)

for j in range(bootsnum):
    resample = np.random.random_integers(low=0,high=(n-1),size=(n,1))
    newdata = np.zeros((n,3))
    for i in range(n): # resampling the data
        newdata[i] = data[resample[i][0]]
    newy,newd,newz = newdata[:,0].reshape(n,1),newdata[:,1].reshape(n,1),newdata[:,2].reshape(n,1)

    # done resample, now each quantile reg
    coeffs = point_est_CDF(tau, y, D, z)

    bootstraps[j] = coeffs
    print "Done with boot ", j+1

std_err_boot = ((bootstraps.var(axis=0))**.5).T

print "(constant, d)"
print std_err_boot

 








































































#### For the CH estimator

Here are the closed form estimates and s.e
(z, constant, d )
[[ -3.82631750e+00  -1.22957005e+02   4.32449845e+01]
 [ -4.32212805e-02  -8.37454238e+01   5.91045862e+01]
 [  4.26810703e-02  -5.45133374e+01   6.26289421e+01]
 [ -4.89186722e-02  -2.57735269e+01   5.67550155e+01]
 [ -4.62594831e-02   9.04085790e-01   5.14684816e+01]
 [  4.99898850e-02   2.30754169e+01   5.67550155e+01]
 [ -1.03045016e-03   5.55970779e+01   4.97063037e+01]
 [ -1.10336141e-02   7.50747428e+01   6.32163348e+01]
 [ -4.03035861e+00   1.32281932e+02   5.14684816e+01]]
(d, constant)
[[ 81.29780233   8.09596442]
 [ 25.06481402   6.13043664]
 [ 16.36996924   5.71100952]
 [ 15.32822643   6.40364167]
 [ 16.13415059   7.99041657]
 [ 21.43447078  12.65072825]
 [ 29.9806923   21.1072406 ]
 [ 29.0285991   21.72032187]
 [ 75.49115232  67.36303567]]


Here are bootstrapped s.e.
[[  3.79839183  22.31612252]
 [  4.49476136  19.44393972]
 [  5.29794377  15.08299683]
 [  6.93464533  16.1082355 ]
 [  7.36231937  14.60997542]
 [ 11.94490409  18.99823864]
 [ 15.5601877   21.27521914]
 [ 15.60717913  18.97398751]
 [ 15.63945686  17.02359154]]





# Monte  Carlo test

MCnum = 100
k=2
MC_coeffs = np.zeros((nq,k,MCnum))

for kj in range(MCnum):
    n = 10000

    eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
    eps_d = (np.random.standard_normal(n)*100).reshape(n,1)
    eps_y = (np.random.standard_normal(n)*100).reshape(n,1)
    strength = .9
    z = 1*(eps_z > 0).reshape(n,1)
    d = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
    y = (1 + 100*d + eps_y).reshape(n,1)

    betaguess = np.array([100,1])
    # Graph of each coefficient by IV quantile
    nq = 9
    quantiles = np.linspace(.1,.9,nq)
    coeffs_AAI = np.zeros((nq,len(betaguess)))
    success = np.zeros(nq)

    for i in range(nq):
        tau = quantiles[i]
        AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z)
        betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
        #coeffs_AAI[i] = betaIVQT.x
        #success[i] = betaIVQT.success
        MC_coeffs[i,:,kj] = betaIVQT.x

    print "Done with monte carlo ", kj +1


MC_mean = ((MC_coeffs.mean(axis=2)))
print MC_mean



