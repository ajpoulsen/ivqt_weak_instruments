\documentclass[letterpaper,12pt]{article}

\usepackage{array}
\usepackage{listings}
\usepackage{geometry}
\geometry{letterpaper,tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1.25in}
\usepackage[format=hang,font=normalsize,labelfont=bf]{caption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{natbib}
\usepackage{setspace}
\usepackage{float,color}
\usepackage[pdftex]{graphicx}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{natbib}

\hypersetup{colorlinks,linkcolor=red,urlcolor=blue,citecolor=red}
%\usepackage{picins}
\theoremstyle{definition}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{derivation}{Derivation} % Number derivations on their own
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}{Proposition} % Number propositions on their own
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\numberwithin{equation}{section}
\bibliographystyle{aer}
\newcommand\ve{\varepsilon}
\makeatletter
\setlength{\@fptop}{5pt}
\makeatother
\newcommand{\norm}[1]{\left\|#1\right\|}
\newcommand{\innerp}[1]{\langle#1\rangle}

\newenvironment{problems}{\begin{list}{}{\setlength{\labelwidth}{.7in}}}{\end{list}}
\newcounter{problempart}
\newenvironment{parts}{\begin{list}{(\alph{problempart})}{\setlength{\itemsep}{0pt}\usecounter{problempart}}}{\end{list}}


\linespread{1.3}


\begin{document}


\title{Quantile Treatment Effects with Endogeneity: A Monte Carlo Comparison of 3 Quantile IV Estimators}

\author{Alexander Poulsen \thanks{Contact me at alexanderjpoulsen@gmail.com. Thanks to Brigham Frandsen at Brigham Young University for his mentoring. Also thanks to BYU's ORCA Grant which supported in part this research and the BYU MCL for training, mentoring, and funding.} \\ Brigham Young University}

\date{May 2015}

\begin{titlepage}
\maketitle

\begin{abstract}
In this paper I use monte carlo simulation to compare three different quantile instrumental variables estimators for treatment effects. I examine the bias, mean squared error, and standard error coverage under varying instrument strengths, sample sizes, and quantiles.
\end{abstract}
\end{titlepage}






\section{Introduction}

Quantile instrumental variables estimators are a relatively new development in the econometric literature. Modern quantile regression was introduced in Koenker and Basset (1978), and has been used in many important applications in which researchers are intereted in learning about the effects of variables on the distribution of an outcome variable, rather than just mean effects. Examples of these applications include changes in U.S. wage structure (Buchinsky 1994), the effect of school quality on student performance (Eide and Showalter 1998), and the relationship between innovation and firm growth (Coad and Rao 2008). 

In these and other applications, mean regression can give misleading results. For example, it could be the case that there are large effects on the bottom tail of the distribution, but little or no effect on the middle and upper tail. In this case, a researcher using mean regression would erroneously conclude that there was some small effect on all points of the distribution \textemdash or perhaps no statistically significant effect at all\textemdash rather than there being large effects on the bottom tail and little effect elsewhere. Results of this kind can have important policy implications, especially as social scientists are increasingy striving to understand and remedy inequality in education, income, and overall quality of life throughout the world.

Many of the important empirical questions we seek to answer in economics become difficult due to endogeneity of regressors. Econometricians began to make progress in adapting quantile regression models to deal with these problems in the early 2000s. Abadie, Angrist, and Imbens (2002) developed an estimator for quantile treatment effects with endogeneity and applied it to look at the effect of JTPA training on the distribution of earnings. Chernozhukov and Hansen (2004) developed a model quantile instrumental variable regression and applied it to look at how 401(k) participation affects wealth distribution. In this paper, I will compare these two methods to the Inverted CDF Estimator (henceforth, ICE) method outlined by Fr\"olich, Melly (2008) and Frandsen (2015). I will compare these through monte carlo simulation, in which I will look at the estimators' bias, mean squared error, variance, and standard error coverage. I will look at these attributes of the estimators under different conditions of weak and strong identification, as well as with differing sample sizes.


\section{Introduction to Three Quantile IV Models}
Traditional quantile regression is estimated with the following equation
$$\hat{\beta}_{\tau} = \underset{\beta}{\operatorname{argmin}}  \sum_{i=1}^n \rho_{\tau} (Y_i - X_i \beta)$$

Where $\rho_{\tau}(x) = x*(\tau - I(x<0))$, $Y$ is an nx1 vector of the outcome variable, $X$ is a nxk matrix of regressors, and $\beta$ is a kx1 vector of coefficients.
This is a generalization of the least absolute deviation (LAD) estimator. In cases of endogeneity, a natural inclination would be to do two stage quantile regression. This, however, would be a biased estimator because the quantile estimator is not a linear estimator.

I will now give an introduction to the three quantile instrumental variable estimators I will consider in this paper. Those interested in learning more of the specifics of the estimators should read the original papers.

\subsection{Abadie, Angrist, and Imbens (2002)}
The estimator developed in Abadie, Angrist, and Imbens (2002) relies on a weighted quantile regression of the following form.
$$\hat{\delta}_{\tau} = \underset{\delta, \beta}{\operatorname{argmin}}  \sum_{i=1}^n E[\kappa_i] \rho_{\tau} (Y_i  - D_i \delta - X_i \beta)$$

Where $Y$ is an nx1 vector of the outcome variable, $D$ is a nx1 vector of an endogenous regressor, $X$ is a nxk matrix of exogenous regressors, and then $\delta$ and $\beta$ are the regression coefficients. Also, $$ \kappa_i = 1 - \frac{D_i*(1-Z_i)}{1- \pi_0(X)_i} - \frac{(1-D_i)*Z_i}{\pi_0(X)_i}$$ with $\pi_0(X)_i = P(Z=1|X_i)$. %It can be shown that $E[\kappa_i] = P(D_1 >D_0 | Y,D,X) $.
The idea behind these weights is that they weigh each observation according to the probability that they are a complier with the natural experiment realized by the instrument. For more details, see Abadie, Angrist, and Imbens (2002).


\subsection{Chernozhukov and Hansen (2004)}
The basic idea behind Chernozhukov and Hansen's estimator is that you do many quantile regressions of $Y - D \delta $ on $X$ and $Z$ over a grid of the coefficient $\delta$, and find the $\delta$ that minimizes the absolute value of the coefficient on $Z$. This works because assuming the exclusion restriction is satisfied, then $Z$ and $Y - D \delta $ should be independent when $\delta$ chosen correctly, because any correlation $Z$ would have with $Y$ would operate only through $D$. 
So the objective function is the following:
$$\underset{\alpha, \beta}{\operatorname{argmin}}  \sum_{i=1}^n \rho_{\tau} (Y_i - D_i \delta_j - Z_i \alpha_j  - X_i \beta_j)$$
Where this regression will be done many times, with $\delta_j$ coming from a predetermined grid of $\delta$. Then to find the treatment effect of $D$ on $Y$, we look at the $\delta_j$ corresponding to the $\alpha_j$ of lowest absolute value.

To do inference for this estimator, I use the robust inference method outlined in Chernozhukov and Hansen (2008)


\subsection{ICE (Inverted CDF Estimator)}

We want to estimate the treatment effect of an endogenous regressor, $D$, on an outcome, $Y$, at different quantiles of the distribution of $Y$. For an individual $i$, we observe only $Y_i$. However, every individual has two hypothetical outcomes, $Y^1_i$, when treated, and $Y^0_i$ when not. The observed $Y_i$ comes from the equation $Y_i(D_i) = D_i Y^1_i + (1-D_i) Y^0_i$. Thus, the treatment affect for one individual can be written as $\delta_i = Y^1_i - Y^0_i$.

We will write our outcome $Y$ as a function of $D$, so that $Y(1)$ is the average value of $Y$ when it has recieved treatment and $Y(0)$ is the value of $Y$ when it has not. We can calculate quantile treatment effects with

$$ \delta (\tau) = Q_{Y(1)}(\tau) - Q_{Y(0)}(\tau)$$

Where the quantile function, $Q_{Y(D)}(\tau) = F_{Y(D)}^{-1}(y)$, is the inverse of the cdf of $Y(D)$.
Now to estimate $Q_{Y(D)}(\tau)$ we will estimate $F_{Y(1)} (y)$, and invert it to get the quantile function.

Consider the following for estimating $Q_{Y(1)}(\tau)$.

$$ \hat{F}_{Y(1)} (y) = \tau $$

Now define: $\tilde{Y}(D) = 1*(Y \leq y)*D $ \newline
Then,
\begin{align*}
\hat{F}_{Y(1)} (y) &= P(Y(1) \leq y) \\
                     &= E[1*(Y(1) \leq y)] \\
                     &= E[ \tilde{Y}(1)] \\
                     &= E[ \tilde{Y}(1) - \tilde{Y}(0) ] \\
        \text{Now this is just the} & \text{ mean treatment of D on } \tilde{Y} \text{, so} \\
                     &= \hat{\beta}^{2SLS}(y) = \frac{\pi ^{RF}(y)}{\pi ^{FS}}
\end{align*}
We estimate $\hat{\beta}^{2SLS}(y)$ with indirect least squares using the following regression equations:
\begin{align*}
    \tilde{Y}(y)_i &= \beta_0 + \pi^{RF}Z_i + \vec{\beta_1}^T \vec{X_i} + \epsilon_i  \newline \\
\text{and} \:   D_i &= \alpha_0 + \pi^{FS}Z_i + \vec{\alpha_1}^T \vec{X_i} + \varepsilon_i \\
\end{align*}

So in order to estimate $Q_{Y(1)}(\tau)$, we simply need to choose $y$ so that
$$ \hat{F}_{Y(1)|c} (y) = \hat{\beta}^{2SLS}(y) = \frac{\pi ^{RF}(y)}{\pi ^{FS}} = \tau $$

In order to find $Q_{Y(0)}(\tau)$, we go through the same procedure, but instead of $D$, we use $D_0 = 1-D$.

For this estimator, no closed-form estimator for the asymptotic variance has been developed, so to do inference, I bootstrap in order to obtain standard errors.

\section{Monte Carlo Simulation}
In order to compare the estimators I use the following data-generating process, repeating the process 2,000 times for each simulation.
\begin{align*}
\varepsilon_z & \sim \mathcal{N}(0,100) \\
\varepsilon_d & \sim \mathcal{N}(0,100) \\
\varepsilon_y & \sim \mathcal{N}(0,100) \\
Z &= I(\varepsilon_z > 0) \\
D &= I(\gamma*Z + \varepsilon_d + \varepsilon_y > 0) \\
Y &= 1 + 100*D + \varepsilon_y
\end{align*}





\subsection{With $n=10,000$}
\begin{table}[ht] 
\caption{With $\tau= 0.1 $ and $n= 10,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 41.135 & 0.1464 & -0.0361 & 0.0233 & 0.3452 & 0.5325 & 0.4836 & \textemdash & 0.947 & 0.952  \\ 
10.0 & 28.0115 & 0.1502 & -0.1237 & 0.0516 & 0.686 & 1.5941 & 1.0879 & \textemdash & 0.9585 & 0.938  \\ 
6.0 & 16.8622 & 0.1927 & -1.1366 & 0.0727 & 1.7315 & 20.0422 & 2.9587 & \textemdash & 0.951 & 0.94  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 




\begin{table}[ht] 
\caption{With $\tau= 0.25 $ and $n= 10,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 41.0984 & 0.16 & -0.0043 & 0.0081 & 0.2411 & 0.2887 & 0.2791 & \textemdash & 0.9075 & 0.9395  \\ 
10.0 & 27.9699 & 0.1783 & 0.0067 & 0.0179 & 0.4041 & 0.5742 & 0.5536 & \textemdash & 0.936 & 0.9435  \\ 
6.0 & 16.9163 & 0.2472 & -0.004 & 0.065 & 1.0193 & 1.5976 & 1.48 & \textemdash & 0.952 & 0.94  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 



\begin{table}[ht] 
\caption{With $\tau= 0.5 $ and $n= 10,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 41.1282 & 0.1372 & 0.0108 & 0.0004 & 0.2168 & 0.39 & 0.252 & \textemdash & 0.976 & 0.9475  \\ 
10.0 & 27.9831 & 0.1353 & 0.0007 & -0.0146 & 0.3911 & 0.6393 & 0.4908 & \textemdash & 0.932 & 0.943  \\ 
6.0 & 16.8934 & 0.2346 & 0.0941 & 0.0575 & 0.887 & 1.3014 & 1.1335 & \textemdash & 0.958 & 0.949  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


\begin{table}[ht] 
\caption{With $\tau= 0.75 $ and $n= 10,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 41.1171 & 0.075 & -0.1169 & -0.0077 & 0.3069 & 1.542 & 0.4452 & \textemdash & 0.966 & 0.9335  \\ 
10.0 & 27.9893 & 0.1074 & -0.1592 & 0.019 & 0.5031 & 2.8385 & 0.7436 & \textemdash & 0.964 & 0.937  \\ 
6.0 & 16.9201 & 0.2117 & -0.2099 & 0.08 & 1.1702 & 5.2627 & 1.7289 & \textemdash & 0.9625 & 0.9415  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


\begin{table}[ht] 
\caption{With $\tau= 0.9 $ and $n= 10,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 41.1037 & -0.0599 & -3.4165 & 0.0442 & 0.6194 & 78.0196 & 1.105 & \textemdash & 0.9165 & 0.945  \\ 
10.0 & 27.9658 & 0.0653 & -3.0147 & 0.1519 & 1.055 & 74.8292 & 1.893 & \textemdash & 0.8875 & 0.943  \\ 
6.0 & 16.9169 & 0.2034 & -3.3717 & 0.2004 & 2.3078 & 89.2977 & 4.3158 & \textemdash & 0.9 & 0.9355  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


With n=10,000
The bias for all of the estimators is pretty small, though the bias of the ICE estimator is fairly consistently smaller than the rest.
For MSE, CH's MSE blows up when the instrument is weak and it is on the extremal quantiles. Perhaps this can be changes by choosing a bigger grid? I'll check that out. 
Standard error coverage is pretty good, but sometimes CH under-covers on the extreme quantiles.




\subsection{With $n=1,000$}

\begin{table}[ht] 
\caption{With $\tau= 0.25 $ and $n= 1,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15 & 12.9929 & 0.2954 & 0.0009 & 0.0665 & 2.1457 & 2.5974 & 2.8582 & \textemdash & 0.956 & 0.9465  \\ 
10 & 8.8979 & 0.4333 & -0.0194 & 0.1539 & 3.6544 & 5.1521 & 5.1839 & \textemdash & 0.971 & 0.952  \\ 
6 & 5.3559 & 0.8616 & -0.3028 & 0.3725 & 10.1486 & 22.8328 & 14.8247 & \textemdash & 0.96 & 0.9465  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


\begin{table}[ht] 
\caption{With $\tau= 0.5 $ and $n= 1,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15 & 13.0503 & 0.3081 & 0.0503 & 0.1143 & 1.9697 & 3.1737 & 2.5625 & \textemdash & 0.9675 & 0.949  \\ 
10 & 8.8709 & 0.3078 & -0.0384 & 0.0735 & 3.1594 & 5.0438 & 4.3406 & \textemdash & 0.9705 & 0.954  \\ 
6 & 5.349 & 0.6711 & 0.07 & 0.3091 & 7.9414 & 13.0718 & 11.7627 & \textemdash & 0.9625 & 0.9565  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


\begin{table}[ht] 
\caption{With $\tau= 0.75 $ and $n= 1,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15 & 13.029 & 0.2611 & -1.8038 & 0.1298 & 2.7597 & 46.2076 & 4.5224 & \textemdash & 0.9425 & 0.9435  \\ 
10 & 8.84 & 0.4243 & -2.0708 & 0.2641 & 4.57 & 59.5364 & 7.3852 & \textemdash & 0.9455 & 0.9505  \\ 
6 & 5.3456 & 0.7492 & -1.7275 & 0.4124 & 10.1073 & 66.1452 & 16.6742 & \textemdash & 0.9465 & 0.9545  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 

\begin{table}[ht] 
\caption{With $\tau= 0.9 $ and $n= 1,000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15 & 12.9831 & 0.4364 & -3.6865 & 0.6067 & 6.6163 & 123.4959 & 12.2279 & \textemdash & 0.8605 & 0.93  \\ 
10 & 8.8133 & 0.7232 & -2.7952 & 0.9047 & 10.8581 & 112.2064 & 19.7117 & \textemdash & 0.87 & 0.924  \\ 
6 & 5.3134 & 1.2977 & -0.9179 & 1.3959 & 22.5968 & 104.3138 & 35.8847 & \textemdash & 0.8795 & 0.932  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 









So for $n=1,000$, all of the estimators with all instrument strengths have fairly low biases, and there isn't much of a pattern of one being more biased than the others, though the ICE seems to be less  biased than the others, and the AAI slightly more.

With MSE, the story gets a little more interesing. AAI consistently has a lower MSE than the other estimators, and CH blows up at the extremal quantiles, especially the upper extremal quantiles. Interesting.

With coverage, CH standard errors are a bit too big for the lower quantiles and then a bit too small for the upper quantiles. ICE bootstrapped standard errors are usually pretty good, but at the very extremes aren't as good.






\section{Conclusions}
In earlier years, quantile regrssion was avoided becuase of the computational difficulties associated with it. With today's computational power, there is no reason not to use quantile regression in most applications, as mean regression can give misleading results to important policy questions. The development of various quantile IV models makes using quantile regression even more possible since we are able to deal with endogeneity.




\section{References}

Abadie, Alberto, Joshua Angrist, and Guido Imbens. ``Instrumental variables estimates of the effect of subsidized training on the quantiles of trainee earnings.'' Econometrica 70.1 (2002): 91-117.

Buchinsky, Moshe. ``Changes in the US wage structure 1963-1987: Application of quantile regression." Econometrica: Journal of the Econometric Society (1994): 405-458.

Chernozhukov, Victor, and Christian Hansen. ``The effects of 401 (k) participation on the wealth distribution: an instrumental quantile regression analysis.'' Review of Economics and statistics 86.3 (2004): 735-751.

Chernozhukov, Victor, and Christian Hansen. ``An IV model of quantile treatment effects.'' Econometrica 73.1 (2005): 245-261.

Chernozhukov, Victor, and Christian Hansen. ``Instrumental variable quantile regression: A robust inference approach." Journal of Econometrics 142.1 (2008): 379-398.

Coad, Alex, and Rekha Rao. ``Innovation and firm growth in high-tech sectors: A quantile regression approach." Research Policy 37.4 (2008): 633-648.

Eide, Eric, and Mark H. Showalter. ``The effect of school quality on student performance: A quantile regression approach." Economics letters 58.3 (1998): 345-350.

Frandsen, Brigham R. ``Treatment Effects With Censoring and Endogeneity." Journal of the American Statistical Association just-accepted (2015): 00-00.

Frölich, Markus, and Blaise Melly. ``Unconditional quantile treatment effects under endogeneity." (2008).

Koenker, Roger, and Gilbert Bassett Jr. ``Regression quantiles." Econometrica: journal of the Econometric Society (1978): 33-50.




\end{document}

