
import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model
import statsmodels.api as smm
from matplotlib import pyplot as plt
from numpy import linalg
import scipy.optimize as spicy
import os 

# /Users/poulsena/Documents/ivqt_weak_instruments

# Making Data
n = 10000

eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
eps_d = (np.random.standard_normal(n)*100).reshape(n,1)
eps_y = (np.random.standard_normal(n)*100).reshape(n,1)
strength = 2
z = 1*(eps_z > 0).reshape(n,1)
d = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
y = (1 + 50*d + eps_y).reshape(n,1)

data = np.array([y,d,z])
data = data[:,:,0].T

"""
# write data
import csv
writer = csv.writer(open("data_2_28_strong.csv", 'w'))
for row in data:
    writer.writerow(row)
#"""

# First Stage
FS = smm.OLS(d, exog=smm.add_constant(z))
fs_fit = FS.fit()
print fs_fit.params
print fs_fit.bse

# Make sure the FS is significant
print fs_fit.params[1]/fs_fit.bse[1]

# Reduced form
RF = smm.OLS(y, exog=smm.add_constant(z))
RF_fit = RF.fit()
print RF_fit.params
print RF_fit.bse

# Here is the IV coeff.
print "Indirect least squares gives: ",  RF_fit.params[1]/fs_fit.params[1]


### Quantile Instrumental Variables Regression (Chernozhukov & Hansen)
def QTreg_solve(betaguess, tau, y, d):
	d = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	u = y - (np.dot(d,betaguess)).reshape(n,1)
	# here is the tilted absolute value function
	u_pos = np.where(u>0)[0]
	u_neg = np.where(u<=0)[0]
	rho_pos = u[u_pos]*tau 
	rho_neg = u[u_neg]*(1-tau)
	return rho_pos.sum() + np.abs(rho_neg.sum())


def IVQTreg(betaguess, tau, y, d, z, gridlen):
    """This function is based off of Chernozhukov & Hansen (2003)."""

    alpha = np.linspace(50-.25*y.var()**.5,50+.25*y.var()**.5,gridlen) # 50 b/c that is the truth
    V = y - d*alpha # creates a matrix (n x gridlen)
    #regressor = np.concatenate((d,z),axis=1)
    betas = np.zeros((gridlen,z.shape[1]+1))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), z)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell')
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,0]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)
    #plot this function to make sure our min is an interior point
    plt.plot(alpha,abs(betas[:,0]))
    plt.plot(rightalpha,abs(betas[rightalpha_i][0]),'ro')
    plt.ylabel('Coefficient of Z')
    plt.xlabel('Coefficient on D')
    plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
    plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
    plt.cla()
    #plt.show()
    print "Done with quantile ", tau
    return rightcoeffs, success


betaguess = np.array([1,1])
gridlen = 500
quantiles = np.linspace(1,9,num=9)
nq = len(quantiles)

k = len(betaguess)
coeffs_CH = np.zeros((nq,k+1)) #(z_minned,constant,d )
successIVQ = np.zeros((nq,gridlen))

os.chdir("/Users/poulsena/Documents/ivqt_weak_instruments/C_H")

for i in range(nq):
    tau = quantiles[i]*.1
    ivqreg = IVQTreg(betaguess, tau, y, d, z, gridlen)
    coeffs_CH[i] = ivqreg[0]
    successIVQ[i] = ivqreg[1]

print successIVQ.sum()
print coeffs_CH

# Variance estimation
k = 2 # amount of variables (d and const)
std_errs = np.zeros((len(quantiles),k))
for j in quantiles:
    tau = j*.1
    j = j-1
    D_cons = np.concatenate((np.ones(n).reshape(n,1),d),axis=1)
    eps_hat = y - np.dot(D_cons,coeffs_CH[j][1:]).reshape(n,1)
    psi = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
    s = (tau - 1*(eps_hat < 0))* psi
    S = np.dot(s.T,s)*(1.0/n)
    S = np.zeros((k,k))
    for i in range(n):
        s_i = s[i].reshape(k,1)
        S += np.dot(s_i,s_i.T)
    S *= (1.0/n)

    sigma_hat = (eps_hat.var())**.5
    h = (4*sigma_hat/(3*n))**(1.0/5)
    def K(x):
        """ Standard Normal """
        return np.exp((-1)*x**2/2)/(2*np.pi)**.5


    DX = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    J = np.zeros((k,k))
    for i in range(n):
        psi_i = psi[i].reshape(k,1)
        DX_i = DX[i].reshape(1,k)
        J += np.dot(psi_i,DX_i)* K(eps_hat[i]/h)*(1/h)
    J *= (1.0/n)

    omega = (1.0/n)*np.dot(np.dot(np.linalg.inv(J),S),np.linalg.inv(J.T))

    std_err =  np.diagonal(omega**.5)

    std_errs[j] = std_err



tau = .2
j = 1

D_cons = np.concatenate((np.ones(n).reshape(n,1),d),axis=1)
eps_hat = y - np.dot(D_cons,coeffs_CH[j][1:]).reshape(n,1)
psi = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
s = (tau - 1*(eps_hat < 0))* psi
S = np.dot(s.T,s)*(1.0/n)
S = np.zeros((k,k))
for i in range(n):
    s_i = s[i].reshape(k,1)
    S += np.dot(s_i,s_i.T)
S *= (1.0/n)

sigma_hat = (eps_hat.var())**.5
h = (4*sigma_hat/(3*n))**(1.0/5)
def K(x):
    """ Standard Normal """
    return np.exp((-1)*x**2/2)/(2*np.pi)**.5


DX = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
J = np.zeros((k,k))
for i in range(n):
    psi_i = psi[i].reshape(k,1)
    DX_i = DX[i].reshape(1,k)
    J += np.dot(psi_i,DX_i)* K(eps_hat[i]/h)*(1/h)
J *= (1.0/n)

omega = (1.0/n)*np.dot(np.dot(np.linalg.inv(J),S),np.linalg.inv(J.T))

std_err =  np.diagonal(omega**.5)




################################################################################################
# Abadie, Angrist, Imbens (2002) approach to quantile instrumental variables
os.chdir("..")
import statsmodels.discrete.discrete_model as sm

def AAIsolve(betaguess, tau, y, d, z):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    #D = X[:,1].reshape(n,1)
    D = d.copy()
    #Xsmall = np.delete(X,1,1) # X without the endogenous regressor, D
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    # This next part now takes care of the weights that are unique to the IV quantile
    # Probit reg z on Xsmall, then get fitted values. This is Pr(z=1|X), call them prZ
    #pZX = sm.Probit(z,Xsmall).fit(disp=0)
    #paramsZX = pZX.params
    #prZ = sm.Probit(z,Xsmall).predict(paramsZX)
    prZ = z.mean()
    
    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)
   
    D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    W = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    Wplus = np.array([np.zeros(len(W)),W]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    
    #print "Percent of negative weights: ", 1.0*(W < 0).sum() / len(W)

    Wplus_pos = Wplus[u_pos]
    Wplus_neg = Wplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


mv /Users/poulsena/Downloads/ivqrstata/ivqreg.ado 

# Graph of each coefficient by IV quantile
nq = 9
quantiles = np.linspace(.1,.9,nq)
coeffs_AAI = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

os.chdir("./AAI")

betaguess = np.array([50,1])

for i in range(nq):
    tau = quantiles[i]
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    coeffs_AAI[i] = betaIVQT.x
    success[i] = betaIVQT.success
    print "Done with quantile ", tau

print success
print coeffs_AAI

# Graphing the objective functions of the endogenous regressor by quantile, 
# holding all other vars constant at their estimated value
def QTobjectiveF(var,tau):
    betaguess[0] = var
    return AAIsolve(betaguess, tau, y, d, z)

k = 0
for i in quantiles: 
    betaguess = coeffs_AAI[k].copy()
    xval = betaguess[0].copy() # the est. on D

    m = 100 # grid size
    offs = .5
    grid = np.linspace(xval-offs*xval,xval+offs*xval,m)

    y_vals = np.zeros(m)
    for j in range(m):
        y_vals[j] = QTobjectiveF(grid[j],i)

    plt.plot(grid,y_vals)
    plt.plot(xval,QTobjectiveF(xval,i),"ro")
    plt.title('Objective Function of the coefficient on D \n at the quantile ' + str(i) +' (using the AAI method)')
    plt.savefig("objfuncfromIVQT_"+str(k+1)+".png")
    plt.cla()
    k += 1



In [413]: omega
Out[413]: 
array([[ 7530689.93448198, -3788524.42580911],
       [-3788524.42580911,  1925217.88935605]])

In [414]: h
Out[414]: 0.42264574513305392

In [415]: omega**.5
Out[415]: 
array([[ 2744.21025697,            nan],
       [           nan,  1387.5222122 ]])








