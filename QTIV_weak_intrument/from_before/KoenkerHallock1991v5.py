""" Replication of Koenker & Hallock (1991) - Alex Poulsen


"""

import numpy as np
from numpy import linalg
import scipy.optimize as spicy
from pandas import Series, DataFrame, Index 
import pandas as pd 
import os 
import time
from matplotlib import pyplot as plt
import random
import statsmodels.discrete.discrete_model as sm


os.chdir("/Users/poulsena/Documents/repos/marriage-and-infant-health/KoenkerHallock1991")
 
# Import the data
data = pd.io.parsers.read_table("marr_notcollapse_forpython.txt")
# get rid of nanners
data = data.dropna()

# Pare down the data a bit more (10% sample)
rows = random.sample(data.index,data.shape[0]/10)
data = data.ix[rows]   

data.bldtest = abs(data.bldtest - 1) # Changing blood test variable to be more like AAI's model when D=Z for compliers

#################### OLS ##########################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)
Z = np.array([np.ones(n),np.array(data.bldtest),black,otherrace,dmeduc,dmage,dmage2,female]).T

a = linalg.inv(np.dot(X.T,X))
#b = np.dot(X.T,Y)
#beta = np.dot(a,b)
betaguess = np.array([2990,70,-213,-131,15,15,0,-112])

def OLS(betaguess, X,Y):
    return ((Y - np.dot(X,betaguess).reshape(n,1))**2).sum()

OLSsolve = lambda betaguess: OLS(betaguess, X, Y)

betaOLS = spicy.fmin(OLSsolve,betaguess)


uhat = Y-betaOLS[0]-(betaOLS[1]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a

print "Using my python code, I get the following results for OLS: "
print "b0 = ", betaOLS[0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", betaOLS[1], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", betaOLS[2], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", betaOLS[3], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", betaOLS[4], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", betaOLS[5], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", betaOLS[6], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", betaOLS[7], "/ s.e.(b7) = ", varcovar[7,7]**.5

reg = pd.ols(y = data.dbirwt, x = data[['married','black','otherrace','dmeduc','dmage','dmage2','female']])
print "Pandas' built in OLS function gives me: "
print reg

z = np.array(data.bldtest).reshape(n,1)
################### 2SLS #####################
Z = np.array([np.ones(n),np.array(data.bldtest),black,otherrace,dmeduc,dmage,dmage2,female]).T

def TSLS(betaguess, Y, X, Z):
    # 1st Stage
    k = X.shape[1]
    delta = np.zeros((k,k))
    success = np.zeros(k+1)
    for i in range(k):
        deltaguess = np.ones(k)# there has to be a better way to do these guesses
        newX = X[:,i].reshape(n,1)
        OLSsolve = lambda deltaguess: OLS(deltaguess,Z,newX)
        powell = spicy.minimize(OLSsolve,deltaguess,method='Powell')
        delta[:,i] = powell.x
        success[i] = powell.success
    
    # 2nd Stage
    Xhat = np.dot(Z,delta) 
    OLSsolve = lambda betaguess: OLS(betaguess, Xhat,Y)
    powell = spicy.minimize(OLSsolve,betaguess,method='Powell')
    beta = powell.x
    success[-1] = powell.success
    
    return beta,success

betaguess = betaOLS
beta2SLS,successq = TSLS(betaguess, Y, X, Z)
print beta2SLS

################## Quantile Regression #####################

# have the guesses for beta be the OLS estimates
betaguess = betaOLS

def QTreg_solve(betaguess, tau, Y, X):
    u = Y - (np.dot(X,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum() + np.abs(rho_neg.sum())

########################################################################

# Graph of each coefficient by quantile

nq = 19
quantiles = np.linspace(.05,.95,nq)
coeffs = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

os.chdir("./coeffbyQuantile")

for i in range(nq):
    tau = quantiles[i]
    QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)
    powell = spicy.minimize(QTsolve,betaguess,method='Powell')
    #beta_min = spicy.fmin(QTsolve,betaguess)
    coeffs[i] = powell.x #beta_min
    success[i] = powell.success

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffs[:,i])
    plt.title("Coeff. of "+variables[i]+" by quantile")
    plt.savefig("byQuantile_"+variables[i]+".png")
    plt.show()

print success



########################################################################
os.chdir("..")
os.chdir("./coeffbyIVquantile")

### Quantile Instrumental Variables Regression (Chernozhukov & Hansen)



def IVQTreg(betaguess, tau, Y, X, Z,gridlen):
    """This function is based off of Chernozhukov & Hansen (2003).
    This is not quite generalized yet. Perhaps later I will add
    how many endogenous regressors you have as an argument, but 
    for now, it assumes there is one."""
    D = X[:,1].reshape(n,1) # The endogenous regressor
    Xnew = np.delete(X,1,1) # The exogenous regressors
    Znew = Z[:,1].reshape(n,1) # Just the instrument

    alpha = np.linspace(beta2SLS[1]-varcovar[1,1]**.5,beta2SLS[1]+varcovar[1,1]**.5,gridlen)
    V = Y - D*alpha
    regressor = np.concatenate((Xnew,Znew),axis=1)
    betas = np.zeros((gridlen,regressor.shape[1]))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), regressor)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell') 
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,-1]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)
    #plot this function to make sure our min is an interior point
    plt.plot(alpha,abs(betas[:,-1]))
    plt.plot(rightalpha,abs(betas[rightalpha_i][-1]),'ro')
    plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
    plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
    plt.cla()
    print "Done with quantile ", tau
    return rightcoeffs, success


##### Now, more analysis with some cool graphs #####
betaguess = beta2SLS
gridlen = 500

k = len(betaguess)
coeffsIVQ = np.zeros((nq,k+1))
successIVQ = np.zeros((nq,gridlen))

t1 = time.time()
for i in range(nq):
    tau = quantiles[i]
    ivqreg = IVQTreg(betaguess, tau, Y, X, Z,gridlen)
    coeffsIVQ[i] = ivqreg[0]
    successIVQ[i] = ivqreg[1]

print successIVQ
totalt = time.time() - t1

print "This took ", totalt/3600, " hours"

# export results to another file
import sys
regout = sys.stdout
f = file('IVQTreg_biggrid.txt','w')
sys.stdout = f
print "Here is the vector with the coefficients from each variables and each quantile"
print '(axis=1) -> constant, black, otherrace, dmeduc, dmage, dmage2, female, marriage'
print '(axis=0) -> .05, .10, .15, ... , .90, .95'
print coeffsIVQ
sys.stdout = regout
f.close()

# graphs

variables = ['constant','black','otherrace','dmeduc','dmage','dmage2','female','bldtest','marriage']
for i in range(9):    
    plt.plot(quantiles,coeffsIVQ[:,i])
    plt.title("Coeff. of "+variables[i]+" by quantile (from Quantile IV Regression)")
    plt.savefig("byIVQuantile_biggrid_"+variables[i]+".png")
    #plt.show()
    plt.cla()



################################################################################################
# Abadie, Angrist, Imbens (2002) approach to quantile instrumental variables
os.chdir("..")

Z = np.array(data.bldtest).reshape(n,1)

def AAIsolve(betaguess, tau, Y, X, Z):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
        Include the endogenous regressor (D) as the 2nd column in the matrix, X.
    """
    D = X[:,1].reshape(n,1)
    Xsmall = np.delete(X,1,1) # X without the endogenous regressor, D

    # This first part is the normal quantile regression part (with the check function)
    u = Y - np.dot(X,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    # This next part now takes care of the weights that are unique to the IV quantile
    # Probit reg Z on Xsmall, then get fitted values. This is Pr(Z=1|X), call them prZ
    pZX = sm.Probit(Z,Xsmall).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(Z,Xsmall).predict(paramsZX)
    # Probit reg Z on Y Xsmall D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((Y,D,Xsmall),axis=1)
    pZYDX = sm.Probit(Z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(Z,YDX).predict(paramsZYDX)
    D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    W = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    Wplus = np.array([np.zeros(len(W)),W]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    Wplus_pos = Wplus[u_pos]
    Wplus_neg = Wplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this






# Graph of each coefficient by IV quantile
nq = 19
quantiles = np.linspace(.05,.95,nq)
coeffs = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

os.chdir("./coeffbyIVQuantileAAI")

for i in range(nq):
    tau = quantiles[i]
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, Y, X, Z)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    coeffs[i] = betaIVQT.x
    success[i] = betaIVQT.success
    print "Done with quantile ", tau

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffs[:,i])
    plt.title("Coeff. of "+variables[i]+" by IV quantile")
    plt.savefig("AAIbyIVQuantile_"+variables[i]+".png")
    plt.cla()
    #plt.show()

print success


# Graphing the objective functions of the endogenous regressor by quantile, 
# holding all other vars constant at their estimated value
def QTobjectiveF(var,tau):
    betaguess[1] = var
    return AAIsolve(betaguess, tau, Y, X, Z)

k = 0
for i in quantiles: 
    betaguess = coeffs[k].copy()
    xval = betaguess[1].copy() # the est. on marriage

    m = 100 # grid size
    offs = .5
    grid = np.linspace(xval-offs*xval,xval+offs*xval,m)

    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    plt.plot(grid,y)
    plt.plot(xval,QTobjectiveF(xval,i),"ro")
    plt.title('Objective Function of the coefficient on Marriage \n at the quantile ' + str(i) +' (using the AAI method)')
    plt.savefig("objfuncfromIVQT_"+str(k+1)+".png")
    plt.cla()
    k += 1








# Learning a bit more about the weights
W = 1 - D*(1-Z)/(1-prZ) - (1-D)*Z/prZ
We = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
Wp = np.array([np.zeros(len(We)),We]).max(axis=0)

(W>0).sum()
W[0:100]

Wp[0:100]

ze = np.where(Wp==0)[0]
W[ze][0:100]
(W[ze]==1).sum()





################################################################################################

# Testing out the IV regression estimator for statsmodels
from statsmodels.sandbox.regression.gmm import IV2SLS

reg = IV2SLS(Y,X,instrument=Z).fit()

betaIV = (reg.params).reshape(8,1)
varcovar_norm = reg.normalized_cov_params # this needs to be multiplied times sigmahat^2

epsilon = Y - np.dot(X,betaIV)
sigmahat2 = np.dot(epsilon.T,epsilon)[0,0]/n
varcovar = varcovar_norm*sigmahat2

betaIV = betaIV[:,0]
print "b0 = ", betaIV[0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", betaIV[1], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", betaIV[2], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", betaIV[3], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", betaIV[4], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", betaIV[5], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", betaIV[6], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", betaIV[7], "/ s.e.(b7) = ", varcovar[7,7]**.5


# this shows all of the attributes
for property, value in vars(reg).iteritems():
    print property, ": ", value


for property, value in vars(p).iteritems():
    print property, ": ", value





