""" Replication of Koenker & Hallock (1991) - Alex Poulsen """


import numpy as np
from numpy import linalg

import scipy.optimize as spicy

from pandas import Series, DataFrame, Index 
import pandas as pd 

import os 

os.chdir("/Users/Alex/Economics/KoenkerHallock1991")
 
# Import the data
data = pd.io.parsers.read_table("marr_notcollapse_forpython.txt")

data = data.dropna()

#################### OLS ##########################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)
a = linalg.inv(np.dot(X.T,X))
b = np.dot(X.T,Y)
beta = np.dot(a,b)
#print "My code gives a coefficient vector of: ", beta
uhat = Y-beta[0]-(beta[1][0]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a
#print "with standard errors: ", np.array([varcovar[0,0]**.5, varcovar[1,1]**.5])

print "Using my python code, I get the following results for OLS: "
print "b0 = ", beta[0,0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", beta[1,0], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", beta[2,0], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", beta[3,0], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", beta[4,0], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", beta[5,0], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", beta[6,0], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", beta[7,0], "/ s.e.(b7) = ", varcovar[7,7]**.5

reg = pd.ols(y = data.dbirwt, x = data[['married','black','otherrace','dmeduc','dmage','dmage2','female']])
print "Pandas' built in OLS function gives me: "
print reg


############### Quantile Regression #####################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)

# have the guesses for beta be the OLS estimates
betaguess = beta

def QTreg_solve(betaguess, tau, Y, X):
    u = Y - np.dot(X,betaguess)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum()+np.abs(rho_neg.sum())

tau = .5
QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)

beta_min = spicy.minimize(QTsolve,betaguess,method = "Nelder-Mead")

beta_min = spicy.minimize(QTreg_solve,betaguess,args=(tau,Y,X),method = "Nelder-Mead")





"""
def
    rho = np.zeros(len(u))
    if u > 0:
        rho = tau*u
    if u <= 0:
        rho = (1-tau)*np.abs(u)
    return rho.sum()
"""














