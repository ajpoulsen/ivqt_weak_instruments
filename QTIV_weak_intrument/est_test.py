
import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model
import statsmodels.api as sm
from matplotlib import pyplot as plt
from numpy import linalg
import scipy.optimize as spicy
import os 


# Making Data
n = 10000

eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
eps_d = (np.random.standard_normal(n)*100).reshape(n,1)
eps_y = (np.random.standard_normal(n)*100).reshape(n,1)

z = 1*(eps_z > 0).reshape(n,1)
d = 1 * (.3*eps_z + eps_d + eps_y > 0).reshape(n,1)
y = (1 + 50*d + eps_y).reshape(n,1)


# First Stage
FS = sm.OLS(d, exog=sm.add_constant(z))
fs_fit = FS.fit()
print fs_fit.params
print fs_fit.bse

# Make sure the FS is significant
print fs_fit.params[1]/fs_fit.bse[1]



### Quantile Instrumental Variables Regression (Chernozhukov & Hansen)
def QTreg_solve(betaguess, tau, y, d):
	d = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	u = y - (np.dot(d,betaguess)).reshape(n,1)
	# here is the tilted absolute value function
	u_pos = np.where(u>0)[0]
	u_neg = np.where(u<=0)[0]
	rho_pos = u[u_pos]*tau 
	rho_neg = u[u_neg]*(1-tau)
	return rho_pos.sum() + np.abs(rho_neg.sum())


def IVQTreg(betaguess, tau, y, d, z, gridlen):
    """This function is based off of Chernozhukov & Hansen (2003)."""

    alpha = np.linspace(50-.25*y.var()**.5,50+.25*y.var()**.5,gridlen) # 50 b/c that is the truth
    V = y - d*alpha # creates a matrix (n x gridlen)
    #regressor = np.concatenate((d,z),axis=1)
    betas = np.zeros((gridlen,z.shape[1]+1))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), z)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell')
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,0]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)
    #plot this function to make sure our min is an interior point
    plt.plot(alpha,abs(betas[:,0]))
    plt.plot(rightalpha,abs(betas[rightalpha_i][0]),'ro')
    plt.ylabel('Coefficient of Z')
    plt.xlabel('Coefficient on D')
    plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
    plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
    plt.cla()
    #plt.show()
    print "Done with quantile ", tau
    return rightcoeffs, success


##### Now, more analysis with some cool graphs #####
betaguess = np.array([0,1])
gridlen = 500
quantiles = np.linspace(.1,.9,num=9)
nq = len(quantiles)

k = len(betaguess)
coeffsIVQ = np.zeros((nq,k+1)) #(z_minned,constant,d )
successIVQ = np.zeros((nq,gridlen))

os.chdir("/Users/poulsena/Documents/QTIV_weak_intrument/C_H")

for i in range(nq):
    tau = quantiles[i]
    ivqreg = IVQTreg(betaguess, tau, y, d, z, gridlen)
    coeffsIVQ[i] = ivqreg[0]
    successIVQ[i] = ivqreg[1]

print successIVQ.sum()
print coeffsIVQ





################################################################################################
# Abadie, Angrist, Imbens (2002) approach to quantile instrumental variables
os.chdir("..")
import statsmodels.discrete.discrete_model as sm

def AAIsolve(betaguess, tau, y, d, z):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
        Include the endogenous regressor (D) as the 2nd column in the matrix, X.
    """
    #D = X[:,1].reshape(n,1)
    D = d.copy()
    #Xsmall = np.delete(X,1,1) # X without the endogenous regressor, D
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    # This next part now takes care of the weights that are unique to the IV quantile
    # Probit reg z on Xsmall, then get fitted values. This is Pr(z=1|X), call them prZ
    
    #pZX = sm.Probit(z,Xsmall).fit(disp=0)
    #paramsZX = pZX.params
    #prZ = sm.Probit(z,Xsmall).predict(paramsZX)
    
   
    D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    W = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    Wplus = np.array([np.zeros(len(W)),W]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    Wplus_pos = Wplus[u_pos]
    Wplus_neg = Wplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this




# Graph of each coefficient by IV quantile
nq = 9
quantiles = np.linspace(.1,.9,nq)
coeffs = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

os.chdir("./AAI")

betaguess = np.array([50,1])

for i in range(nq):
    tau = quantiles[i]
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    coeffs[i] = betaIVQT.x
    success[i] = betaIVQT.success
    print "Done with quantile ", tau

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffs[:,i])
    plt.title("Coeff. of "+variables[i]+" by IV quantile")
    plt.savefig("AAIbyIVQuantile_"+variables[i]+".png")
    plt.cla()
    #plt.show()

print success


# Graphing the objective functions of the endogenous regressor by quantile, 
# holding all other vars constant at their estimated value
def QTobjectiveF(var,tau):
    betaguess[1] = var
    return AAIsolve(betaguess, tau, y, d, z)

k = 0
for i in quantiles: 
    betaguess = coeffs[k].copy()
    xval = betaguess[1].copy() # the est. on marriage

    m = 100 # grid size
    offs = .5
    grid = np.linspace(xval-offs*xval,xval+offs*xval,m)

    y_vals = np.zeros(m)
    for j in range(m):
        y_vals[j] = QTobjectiveF(grid[j],i)

    plt.plot(grid,y_vals)
    plt.plot(xval,QTobjectiveF(xval,i),"ro")
    plt.title('Objective Function of the coefficient on Marriage \n at the quantile ' + str(i) +' (using the AAI method)')
    plt.savefig("objfuncfromIVQT_"+str(k+1)+".png")
    plt.cla()
    k += 1










