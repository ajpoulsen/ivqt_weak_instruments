import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model
import statsmodels.api as smm
from matplotlib import pyplot as plt
from numpy import linalg
import scipy.optimize as spicy
import os 


### Quantile Instrumental Variables Regression (Chernozhukov & Hansen)
def QTreg_solve(betaguess, tau, y, d):
	d = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	u = y - (np.dot(d,betaguess)).reshape(n,1)
	# here is the tilted absolute value function
	u_pos = np.where(u>0)[0]
	u_neg = np.where(u<=0)[0]
	rho_pos = u[u_pos]*tau 
	rho_neg = u[u_neg]*(1-tau)
	return rho_pos.sum() + np.abs(rho_neg.sum())

def K(x):
    """ Standard Normal """
    return np.exp((-1)*x**2/2)/(2*np.pi)**.5


def IVQT_CH(betaguess, tau, y, d, z, gridlen,graphs = False, var=False):
    """This function is based off of Chernozhukov & Hansen (2003)."""
    truth = 100
    alpha = np.linspace(truth-.25*y.var()**.5,truth+.25*y.var()**.5,gridlen) 
    V = y - d*alpha # creates a matrix (n x gridlen)
    #regressor = np.concatenate((d,z),axis=1)
    betas = np.zeros((gridlen,z.shape[1]+1))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), z)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell')
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,0]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)

    # Estimating Variance/ Confidence Intervals
    std_err = 0
    if var == True:
	    k = 2
	    D_cons = np.concatenate((np.ones(n).reshape(n,1),d),axis=1)
	    eps_hat = y - np.dot(D_cons,rightcoeffs[1:]).reshape(n,1)
	    psi = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
	    s = (tau - 1*(eps_hat < 0))* psi
	    S = np.dot(s.T,s)*(1.0/n)
	    S = np.zeros((k,k))
	    for i in range(n):
	        s_i = s[i].reshape(k,1)
	        S += np.dot(s_i,s_i.T)
	    S *= (1.0/n)

	    sigma_hat = (eps_hat.var())**.5
	    h = (4*sigma_hat**5/(3*n))**(1.0/5)

	    DX = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	    J = np.zeros((k,k))
	    for i in range(n):
	        psi_i = psi[i].reshape(k,1)
	        DX_i = DX[i].reshape(1,k)
	        J += np.dot(psi_i,DX_i)* K(eps_hat[i]/h)*(1/h)
	    J *= (1.0/n)

	    omega = (1.0/n)*np.dot(np.dot(np.linalg.inv(J),S),np.linalg.inv(J.T))
	    std_err =  np.diagonal(omega**.5)
	    c = 1.96 # from the T-table
	    # Confidence Intervals
	    upper = rightalpha + c*std_err[0]
	    lower = rightalpha - c*std_err[0]
    #plot this function to make sure our min is an interior point
    if graphs == True:
        plt.plot(alpha,abs(betas[:,0]))
        plt.plot(rightalpha,abs(betas[rightalpha_i][0]),'ro')
        plt.axvspan(lower, upper, facecolor='#00cccc')
        plt.ylabel('Coefficient of Z')
        plt.xlabel('Coefficient on D')
        plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
        plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
        plt.cla()
        #plt.show()
    #print "Done with quantile ", tau
    return rightcoeffs, std_err, success.sum() == gridlen



# Monte Carlo Simulations
n = 1000


betaguess = np.array([1,100])
gridlen = 80
k = len(betaguess)
tau = .5
mc = 100
MC_coeffs = np.zeros((mc,k))
ERROR = np.zeros(mc)
true = np.zeros(mc)
for j in range(mc):

	scale = 100
	Y_0 = np.exp(np.random.standard_normal(n)).reshape(n,1)
	Y_1 = scale*Y_0
	eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
	Z= 1*(eps_z <.5)

	q_25 = np.percentile(Y_0,25)
	q_5 = np.percentile(Y_0,50)
	q_75 = np.percentile(Y_0,75)

	qT_5 = np.percentile(Y_1,50)

	DE = 1*(Y_0<q_25)
	NT = 1*((Y_0>q_25) & (Y_0<q_5))
	AT = 1*((Y_0>q_5) & (Y_0<q_75))
	C = 1*(Y_0>q_75)

	D = np.zeros(n).reshape(n,1)
	for i in range(n):
		#if DE[i] == 1: D[i] = 1-Z[i]
		if NT[i] == 1: D[i] = 0
		if AT[i] == 1: D[i] = 1
		if  C[i] == 1: D[i] = Z[i]

	Y = Y_0*(1-D) + Y_1*D

	ivqreg = IVQT_CH(betaguess, tau, Y, D, Z, gridlen)
	true_ = qT_5 - q_5 # The true ATE
	true[j] = true_
	MC_coeffs[j] = ivqreg[0][1:]
	ERROR[j] = (true_ - MC_coeffs[j,1])**2
	print "Done with simulation ", j+1

MSE = ERROR.mean()
bias = MC_coeffs[:,1].mean() - true.mean()

print MSE
print bias




# Testing the model the the original data generating process
n = 10000
betaguess = np.array([1,100])
gridlen = 80
k = len(betaguess)
tau = .5
mc = 100
MC_coeffs = np.zeros((mc,k))
SQ_ERR = np.zeros(mc)
true = np.zeros(mc)
for j in range(mc):

	eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
	eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
	eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
	strength = .9
	Z = 1*(eps_z > 0).reshape(n,1)
	D = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
	Y = (1 + 100*D + eps_y).reshape(n,1)

	ivqreg = IVQT_CH(betaguess, tau, Y, D, Z, gridlen)
	true_ = 100 # The true ATE
	true[j] = true_
	MC_coeffs[j] = ivqreg[0][1:]
	SQ_ERR[j] = (true_ - MC_coeffs[j,1])**2
	print "Done with simulation ", j+1

MSE = SQ_ERR.mean()
bias = MC_coeffs[:,1].mean() - true.mean()

print MSE
print bias












# Monte Carlo Simulations
n = 10000


betaguess = np.array([1,100])
gridlen = 80
k = len(betaguess)
tau = .5
mc = 1000
MC_coeffs = np.zeros((mc,k))
ERROR = np.zeros(mc)
true = np.zeros(mc)
for j in range(mc):

	scale = 100
	Y_0 = np.exp(np.random.standard_normal(n)).reshape(n,1)
	Y_1 = scale*Y_0
	eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
	Z= 1*(eps_z <.5)

	q_25 = np.percentile(Y_0,25)
	q_5 = np.percentile(Y_0,50)
	q_75 = np.percentile(Y_0,75)

	qT_5 = np.percentile(Y_1,50)

	NT = 1*(Y_0<q_25)
	AT = 1*((Y_0>q_25) & (Y_0<q_5))
	C = 1*(Y_0>q_5)
	#C = 1*(Y_0>q_75)

	D = np.zeros(n).reshape(n,1)
	for i in range(n):
		#if DE[i] == 1: D[i] = 1-Z[i]
		if NT[i] == 1: D[i] = 0
		if AT[i] == 1: D[i] = 1
		if  C[i] == 1: D[i] = Z[i]

	Y = Y_0*(1-D) + Y_1*D

	ivqreg = IVQT_CH(betaguess, tau, Y, D, Z, gridlen)
	true_ = qT_5 - q_5 # The true ATE
	true[j] = true_
	MC_coeffs[j] = ivqreg[0][1:]
	ERROR[j] = (true_ - MC_coeffs[j,1])**2
	#print "Done with simulation ", j+1

MSE = ERROR.mean()
bias = MC_coeffs[:,1].mean() - true.mean()

print MSE
print bias





# AAI estimator
import statsmodels.discrete.discrete_model as sm


def AAIsolve(betaguess, tau, y, d, z, Wplus):
    D = (d.copy()).reshape(n,1)
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = Wplus[u_pos]
    Wplus_neg = Wplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def IVQT_AAI(betaguess, tau, y, d, z):
	""" This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
	    estimator.
	"""
	D = d.copy()
	D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
	# This part takes care of the weights
	# This is Pr(z=1|X), call them prZ
	X = np.ones(n).reshape(n,1)
	pZX = sm.Probit(z,X).fit(disp=0)
	paramsZX = pZX.params
	prZ = sm.Probit(z,X).predict(paramsZX)
	#prZ = z.mean()

	# Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
	YDX = np.concatenate((y,D_cons,y**2,y**3,y*(D_cons[:,0].reshape(n,1))),axis=1)
	pZYDX = sm.Probit(z,YDX).fit(disp=0)
	paramsZYDX = pZYDX.params
	fvZ = sm.Probit(z,YDX).predict(paramsZYDX)

	D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
	# Now here is the equation for the weights, as suggested
	W = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
	Wplus = np.array([np.zeros(len(W)),W]).max(axis=0) # This makes all negatives into zeros
	# dividing the weights to go with the positive and negative values of u
	#print "Percent of negative weights: ", 1.0*(W < 0).sum() / len(W)
	AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, Y, D, Z, Wplus)
	betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')

	return betaIVQT

#
    
tau = .5

n = 10000

betaguess = np.array([100,1])
k = len(betaguess)
tau = .5
mc = 1000
MC_coeffs = np.zeros((mc,k))
SQ_ERR = np.zeros(mc)
true = np.zeros(mc)
for j in range(mc):

	eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
	eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
	eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
	strength = .9
	Z = 1*(eps_z > 0).reshape(n,1)
	D = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
	Y = (1 + 100*D + eps_y).reshape(n,1)

	betaIVQT = IVQT_AAI(betaguess, tau, Y, D, Z)
	true_ = 100 # The true ATE
	true[j] = true_
	MC_coeffs[j] = betaIVQT.x
	SQ_ERR[j] = (true_ - betaIVQT.x[0])**2
	#print "Done with simulation ", j+1

MSE = SQ_ERR.mean()
bias = MC_coeffs[:,0].mean() - true.mean()

print MSE
print bias







#### CDF Monte Carlo


  
tau = .5

n = 10000

betaguess = np.array([100,1])
k = len(betaguess)
tau = .5
mc = 1000
MC_coeffs = np.zeros((mc,1))
SQ_ERR = np.zeros(mc)
true = np.zeros(mc)
for j in range(mc):

    eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
    eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
    eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
    strength = .9
    Z = 1*(eps_z > 0).reshape(n,1)
    D = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
    Y = (1 + 100*D + eps_y).reshape(n,1)

    betaIVQT = IVQT_CDF(tau, Y, D, Z)
    true_ = 100 # The true ATE
    true[j] = true_
    MC_coeffs[j] = betaIVQT
    SQ_ERR[j] = (true_ - betaIVQT)**2
    #print "Done with simulation ", j+1

MSE = SQ_ERR.mean()
bias = MC_coeffs[:,0].mean() - true.mean()

print MSE
print bias
