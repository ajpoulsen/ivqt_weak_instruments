# Results 

BOOTS

[ 0.11877208  0.61197239  0.32181042]
[ 11.33457047   6.81303018   9.98160637]
[ 11.32046366   6.43851997   9.87804442]
6.31268490341
[ 0.965   0.9345  0.9445]
[ 0.968   0.9955]

In [253]: n
Out[253]: 500

In [254]: tau
Out[254]: 0.5

In [255]: strength
Out[255]: 10


######################################################################
BOOTS
[ 0.06093502  0.41135622  0.18825987]
[ 5.77709668  3.52760555  4.87811708]
[ 5.7733836   3.35839161  4.8426753 ]
8.86463033389
[ 0.9525  0.926   0.9405]
[ 0.96    0.9985]

In [11]: n
Out[11]: 1000

In [12]: tau
Out[12]: 0.5

In [13]: strength
Out[13]: 10

######################################################################
[-0.08088983  0.36819949  0.08025368]
[ 4.90913078  3.55319167  4.97266662]
[ 4.90258762  3.41762081  4.96622596]
8.85569697926
[ 0.966   0.998   0.9535]

In [3]: tau
Out[3]: 0.25

In [4]: n
Out[4]: 1000

In [5]: strength
Out[5]: 10


######################################################################

[-1.65057357  0.46542249  0.18877501]
[ 40.82896095   6.65568259  10.53240834]
[ 38.10456784   6.4390645   10.49677234]
8.88426685309
[ 0.959   0.9945  0.934 ]

In [7]: n
Out[7]: 1000

In [8]: tau
Out[8]: 0.1

In [9]: strength
Out[9]: 10


######################################################################

[-2.23920934  0.32114909  0.13355634]
[ 61.38690896   4.46965274   7.31031762]
[ 56.37285048   4.36651601   7.29248033]
8.85723338468
[ 0.948   0.9995  0.948 ]

In [7]: tau
Out[7]: 0.75

In [8]: n
Out[8]: 1000


######################################################################

[-2.62037478  0.75280266  0.98235998]
[ 108.67604685   11.01973135   19.26527846]
[ 101.80968288   10.4530195    18.30024734]
8.85475640515
[ 0.871   0.993   0.9245]

In [3]: n
Out[3]: 1000

In [4]: tau
Out[4]: 0.9


######################################################################


[-2.14694635  0.64442977  0.33692372]
[ 71.25497865   8.48738381  13.3257718 ]
[ 66.6456       8.07209408  13.2122542 ]
6.2271333058
[ 0.9445  0.999   0.9505]

In [6]: tau
Out[6]: 0.75

In [7]: n
Out[7]: 1000

In [8]: strength
Out[8]: 7

######################################################################

[-1.15711196  1.37897822  1.51640057]
[ 102.77192016   19.3077973    31.06587131]
[ 101.43301208   17.40621638   28.76640062]
6.2272736996
[ 0.8695  0.986   0.916 ]

In [10]: tau
Out[10]: 0.9

In [11]: strength
Out[11]: 7

In [12]: n
Out[12]: 1000

######################################################################

[-0.03026517  0.46409079  0.08591693]
[ 9.56617968  5.80290375  8.86969406]
[ 9.5652637   5.58752349  8.86231234]
6.2043772689
[ 0.9675  0.9995  0.9585]

In [11]: tau
Out[11]: 0.5

In [12]: n
Out[12]: 1000

In [13]: strength
Out[13]: 7

######################################################################
SWITCHED AAI AND CH

[ 0.91250428  0.18260011  0.46652624]
[ 11.4553134   18.03616811  17.0255707 ]
[ 10.62264933  18.00282531  16.80792397]
4.47204204841
[ 0.998   0.9665  0.9625]

In [15]: tau
Out[15]: 0.5

In [16]: n
Out[16]: 1000

In [17]: strength
Out[17]: 5

In [18]: t_stats.min()
Out[18]: 0.89158729304625262

######################################################################
######################################################################

Bias, MSE, Coverage


tau =  0.9
n =  1000
[[  15.       13.0219    0.4001   -3.9787    0.5385    6.328   123.7954
    11.5161    0.997     0.8755    0.9335]
 [  10.        8.8398    0.9068   -2.3569    1.0501   11.3768  106.4621
    19.2217    0.9905    0.868     0.927 ]
 [   5.        4.4411    1.9836    0.8376    1.9089   30.7721  104.6579
    48.172     0.9795    0.873     0.919 ]]


tau =  0.1
n =  10000
[[ 15.      41.1468   0.1463  -0.0282   0.0396   0.3495   0.5248   0.4975
    0.997    0.9405   0.9475]
 [ 10.      28.0063   0.1395  -0.1209   0.0544   0.6686   1.696    1.1001
    0.9975   0.958    0.9475]
 [  5.      14.1167   0.2982  -1.3805   0.227    2.5887  29.8476   4.5111
    0.997    0.939    0.9235]]


tau =  0.25
n =  10000
[[ 15.      41.1534   0.1566  -0.0116  -0.0055   0.2201   0.2844   0.2677
    0.997    0.9175   0.9495]
 [ 10.      27.9574   0.1608  -0.0173   0.002    0.402    0.5568   0.5452
    0.999    0.9415   0.938 ]
 [  5.      14.0751   0.2613   0.0131   0.1216   1.4215   2.4117   2.1079
    0.9995   0.945    0.9355]]



tau =  0.5
n =  10000
[[ 15.      41.1467   0.1439   0.0315   0.0153   0.2114   0.4011   0.2539
    0.9965   0.9705   0.945 ]
 [ 10.      27.9987   0.1597   0.0421   0.0345   0.3711   0.6015   0.471
    0.999    0.9325   0.943 ]
 [  5.      14.1195   0.2369   0.0706   0.0522   1.2094   1.819    1.6937
    0.9985   0.954    0.9465]]


tau =  0.75
n =  10000
[[ 15.      41.1014   0.0224  -0.067    0.0246   0.2766   1.4861   0.4316
    0.9995   0.9675   0.945 ]
 [ 10.      28.0048   0.0863  -0.1164   0.0712   0.5016   2.5432   0.7761
    0.999    0.949    0.9335]
 [  5.      14.0628   0.1274  -0.4098   0.0175   1.4619   8.3805   2.3438
    0.9995   0.9645   0.945 ]]


tau =  0.9
n =  10000
[[ 15.      41.1281  -0.1819  -3.476    0.0428   0.6237  81.2714   1.0977
    0.9985   0.9065   0.9425]
 [ 10.      27.9604  -0.0913  -3.3183   0.0643   0.8929  79.0308   1.7571
    0.9995   0.913    0.942 ]
 [  5.      14.0966   0.2828  -3.2553   0.3326   3.2548  90.1977   6.3759
    1.       0.8915   0.929 ]]


######################################################################
######################################################################
######################################################################


#  NEW RESULTS 

\begin{table}[ht] 
\caption{With $\tau= 0.1 $ and $n= 1000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & FFM & AAI & CH & FFM & AAI & CH & FFM \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 13.0537 & 0.3166 & -0.2697 & 0.1148 & 3.7266 & 6.9479 & 4.878 & 0.7135 & 0.9615 & 0.949  \\ 
10.0 & 8.8282 & 0.4814 & -1.7482 & 0.1425 & 7.284 & 41.7094 & 10.3544 & 0.8065 & 0.9545 & 0.946  \\ 
6.0 & 5.3788 & 1.2722 & -1.1551 & nan & 18.2675 & 72.8205 & nan & 0.806 & 0.923 & 0.9305  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 



    \begin{table}[ht] 
\caption{With $\tau= 0.5 $ and $n= 10000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & FFM & AAI & CH & FFM & AAI & CH & FFM \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
10.0 & 27.997 & 0.1512 & 0.048 & 0.0235 & 0.3697 & 0.5952 & 0.4376 & 0.0 & 0.934 & 0.9575  \\ 
7.0 & 19.6658 & 0.1696 & 0.0269 & 0.0116 & 0.6724 & 1.067 & 0.9013 & 0.0 & 0.952 & 0.942  \\ 
4.0 & 11.281 & 0.3313 & 0.1016 & 0.0757 & 1.82 & 2.6728 & 2.4333 & 0.0 & 0.9545 & 0.951  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


    \begin{table}[ht] 
\caption{With $\tau= 0.1 $ and $n= 1000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & FFM & AAI & CH & FFM & AAI & CH & FFM \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
10.0 & 8.82 & 0.5199 & -1.5601 & 0.1661 & 7.0106 & 37.2429 & 10.0057 & 0.0 & 0.953 & 0.95  \\ 
7.0 & 6.2284 & 1.0145 & -1.7371 & 0.5771 & 13.9859 & 64.5728 & 19.5361 & 0.0 & 0.948 & 0.945  \\ 
4.0 & 3.587 & 2.2125 & 1.3948 & nan & 37.5645 & 101.1788 & nan & 0.0 & 0.9225 & 0.8205  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 


  \begin{table}[ht] 
\caption{With $\tau= 0.1 $ and $n= 10000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 41.1693 & 0.0163 & -0.0413 & 0.0174 & 0.3768 & 0.5253 & 0.5063 & \textemdash & 0.9475 & 0.9445  \\ 
10.0 & 27.996 & 0.0407 & -0.1279 & 0.0295 & 0.7578 & 1.5822 & 1.0498 & \textemdash & 0.952 & 0.945  \\ 
6.0 & 16.8914 & 0.2029 & -0.8744 & 0.1362 & 1.9875 & 15.9556 & 3.0371 & \textemdash & 0.947 & 0.9435  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 
    

    \begin{table}[ht] 
\caption{With $\tau= 0.1 $ and $n= 1000 $}

    \centering 
    \scalebox{.9}{

    \begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\ [0.5ex]  
    
    \hline % inserts single horizontal line    
15.0 & 12.9778 & 0.2256 & -0.3544 & 0.0329 & 3.636 & 6.3473 & 4.9239 & \textemdash & 0.967 & 0.9435  \\ 
10.0 & 8.8516 & 0.5351 & -1.6182 & 0.2698 & 7.291 & 40.6869 & 10.2351 & \textemdash & 0.957 & 0.936  \\ 
6.0 & 5.3634 & 1.4266 & -0.891 & nan & 18.6556 & 67.1407 & nan & \textemdash & 0.937 & 0.9375  \\ [1ex] 

    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 

  

