
betaguess = np.array([0,0])
tau = .5
# Monte Carlo Tests
n = 1000
mc = 1000

strengths = [20]
#The matrices of results
biases = np.zeros((3,3))
MSEs = np.zeros((3,3))
Vars = np.zeros((3,3))
coverages = np.zeros((3,3))
avg_FSs = np.zeros((3,1))


for gamma in strengths:
    t_stats = np.zeros(mc)
    coeffs = np.zeros((mc,3)) #(CH, AAI, CDF)
    st_errs = np.zeros((mc,3))
    st_errs_B = np.zeros((mc,3))

    for i in range(mc):
        #strength = 20
        coeff = 100
        #while t_stats[i] < 2.1:
        eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
        z = 1*(eps_z > 0).reshape(n,1)
        d = 1 * (gamma*z + eps_d + eps_y > 0).reshape(n,1)
        y = (1 + coeff*d + eps_y).reshape(n,1)

        # First Stage
        FS = smm.OLS(d, exog=smm.add_constant(z))
        fs_fit = FS.fit()
        # Make sure the FS is significant
        t_stats[i] = fs_fit.params[1]/fs_fit.bse[1]

        print i
        ###########################################
        #CH = IVQT_CH(betaguess, tau, y, d, z, gridlen)
        
        AAI = IVQT_AAI(betaguess, tau, y, d, z)
        #print AAI

        coeffs[i,0], st_errs[i,0] = AAI[0][0], AAI[1][0]
        #coeffs[i,1], st_errs[i,1] = CH[0][0], CH[1][0] 
        #coeffs[i,2], st_errs[i,2] = CDF[0], CDF[1]


    avg_FS = t_stats.mean()
    bias =  coeffs.mean(axis=0) - coeff
    SQ_ERR = (coeffs - coeff)**2
    MSE = SQ_ERR.mean(axis=0)
    Var = MSE - bias**2

    coverage_right = (coeffs < coeff + 1.96*st_errs)
    coverage_left  = (coeffs > coeff - 1.96*st_errs)
    coverage = (coverage_left*coverage_right).mean(axis=0) # This should be about 95%

    print bias
    print MSE
    print Var
    print avg_FS
    print coverage



#########################################################

# Exogenous treatment

betaguess = np.array([0,0])
tau = .1
# Monte Carlo Tests
n = 1000
mc = 1000

strengths = [20]
#The matrices of results
biases = np.zeros((3,3))
MSEs = np.zeros((3,3))
Vars = np.zeros((3,3))
coverages = np.zeros((3,3))
avg_FSs = np.zeros((3,1))


for gamma in strengths:
    t_stats = np.zeros(mc)
    coeffs = np.zeros((mc,3)) #(CH, AAI, CDF)
    st_errs = np.zeros((mc,3))
    st_errs_B = np.zeros((mc,3))

    for i in range(mc):
        #strength = 20
        coeff = 100
        #while t_stats[i] < 2.1:
        eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
        #z = 1*(eps_z > 0).reshape(n,1)
        d = 1 * (eps_d > 0).reshape(n,1)
        y = (1 + coeff*d + eps_y).reshape(n,1)

        z = d

        # First Stage
        FS = smm.OLS(d, exog=smm.add_constant(z))
        fs_fit = FS.fit()
        # Make sure the FS is significant
        t_stats[i] = fs_fit.params[1]/fs_fit.bse[1]

        print i
        ###########################################
        #CH = IVQT_CH(betaguess, tau, y, d, z, gridlen)
        z = d
        AAI = IVQT_AAI(betaguess, tau, y, d, z)
        #print AAI

        coeffs[i,0], st_errs[i,0] = AAI[0][0], AAI[1][0]
        #coeffs[i,1], st_errs[i,1] = CH[0][0], CH[1][0] 
        #coeffs[i,2], st_errs[i,2] = CDF[0], CDF[1]


    avg_FS = t_stats.mean()
    bias =  coeffs.mean(axis=0) - coeff
    SQ_ERR = (coeffs - coeff)**2
    MSE = SQ_ERR.mean(axis=0)
    Var = MSE - bias**2

    coverage_right = (coeffs < coeff + 1.96*st_errs)
    coverage_left  = (coeffs > coeff - 1.96*st_errs)
    coverage = (coverage_left*coverage_right).mean(axis=0) # This should be about 95%

    print bias
    print MSE
    print Var
    print avg_FS
    print coverage




#####################################

# REALLY strong instrument 



betaguess = np.array([0,0])
tau = .5
# Monte Carlo Tests
n = 1000
mc = 1000

strengths = [20]
#The matrices of results
biases = np.zeros((3,3))
MSEs = np.zeros((3,3))
Vars = np.zeros((3,3))
coverages = np.zeros((3,3))
avg_FSs = np.zeros((3,1))


for gamma in strengths:
    t_stats = np.zeros(mc)
    coeffs = np.zeros((mc,3)) #(CH, AAI, CDF)
    st_errs = np.zeros((mc,3))
    st_errs_B = np.zeros((mc,3))

    for i in range(mc):
        #strength = 20
        coeff = 100
        #while t_stats[i] < 2.1:
        eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
        z = 1*(eps_z > 0).reshape(n,1)
        d = 1 * (gamma*z + eps_d + eps_y > 0).reshape(n,1)
        y = (1 + coeff*d + eps_y).reshape(n,1)

        #####
        d = z.copy()

        d[0,0] = 1 - d[0,0]

        # First Stage
        FS = smm.OLS(d, exog=smm.add_constant(z))
        fs_fit = FS.fit()
        # Make sure the FS is significant
        t_stats[i] = fs_fit.params[1]/fs_fit.bse[1]

        print i
        ###########################################
        #CH = IVQT_CH(betaguess, tau, y, d, z, gridlen)
        
        AAI = IVQT_AAI(betaguess, tau, y, d, z)
        #print AAI

        coeffs[i,0], st_errs[i,0] = AAI[0][0], AAI[1][0]
        #coeffs[i,1], st_errs[i,1] = CH[0][0], CH[1][0] 
        #coeffs[i,2], st_errs[i,2] = CDF[0], CDF[1]


    avg_FS = t_stats.mean()
    bias =  coeffs.mean(axis=0) - coeff
    SQ_ERR = (coeffs - coeff)**2
    MSE = SQ_ERR.mean(axis=0)
    Var = MSE - bias**2

    coverage_right = (coeffs < coeff + 1.96*st_errs)
    coverage_left  = (coeffs > coeff - 1.96*st_errs)
    coverage = (coverage_left*coverage_right).mean(axis=0) # This should be about 95%

    print bias
    print MSE
    print Var
    print avg_FS
    print coverage





