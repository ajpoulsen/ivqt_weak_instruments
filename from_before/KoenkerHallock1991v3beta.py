""" Replication of Koenker & Hallock (1991) 

- This version (v3) has an investigation of why it takes forever for 
the quantile regression to converge when you use lots
of fixed effects dummy variables, wheras v2 has the graphs of coeffs
by quantile, without the fixed effects.



Written by Alex Poulsen """


import numpy as np
from numpy import linalg
import scipy.optimize as spicy
from pandas import Series, DataFrame, Index 
import pandas as pd 
import os 
import time
import sys
import random
from matplotlib import pyplot as plt
import statsmodels.discrete.discrete_model as sm

os.chdir("/Users/poulsena/Documents/repos/marriage-and-infant-health/KoenkerHallock1991")

# change the output directory
"""
orig_stdout = sys.stdout
f = file("KH1991v3_3.txt","w")
sys.stdout = f
"""
   
    
# Import the data
data = pd.io.parsers.read_table("marr_notcollapse_forpython.txt")
# get rid of nanners
data = data.dropna()

# Pare down the data a bit more (10% sample)
rows = random.sample(data.index,data.shape[0]/10)
data = data.ix[rows]

"""
################################## Some experimenting 
for i in [1,4,5,9,10,11,13,18,19,20,22,23,24,25,27,28,29,30,32,37,39,40,41,45,53]:
    data = data[data.state != i]
# the above eliminates all states whose bldtest status does not vary

##################################
"""

# creating dummy variables for year and state
statedummies = pd.core.reshape.get_dummies(data['state'])
yeardummies = pd.core.reshape.get_dummies(data['year'])
data = data.join(statedummies)
data = data.join(yeardummies)

data['yeartime'] = data['year']-1968

#################### OLS ##########################
n = data.shape[0]
# Converting all of the data fom pandas objects to numpy arrays
# Here are each of the important variables
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
yeartime = np.array(data.yeartime).reshape(n,1)
# Here are all of the dummy variables - so many!!
statedum = np.array(statedummies)
yeardum = np.array(yeardummies)
state_timedum = statedum*yeartime
state_time2dum = statedum*(yeartime)**2
# now putting them all together
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
X = np.concatenate((X,statedum[:,1:]), axis=1)
X = np.concatenate((X,yeardum[:,1:]), axis=1)
#X = np.concatenate((X,state_timedum[:,1:]), axis=1)
#X = np.concatenate((X,state_time2dum[:,1:]), axis=1)

Y = np.array(data.dbirwt).reshape(n,1)
a = linalg.inv(np.dot(X.T,X))
#b = np.dot(X.T,Y)
#beta = np.dot(a,b)
betaguess = np.array([2990,70,-213,-131,15,15,0,-112])
dumguess = np.zeros(X.shape[1]-8)
betaguess = np.concatenate((betaguess,dumguess),axis=1)

def OLS(betaguess, X,Y):
    return ((Y - np.dot(X,betaguess).reshape(n,1))**2).sum()

OLSsolve = lambda betaguess: OLS(betaguess, X, Y)

beta = spicy.fmin(OLSsolve,betaguess)


uhat = Y-beta[0]-(beta[1]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a

print "Using my python code, I get the following results for OLS: "
print "b0 = ", beta[0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", beta[1], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", beta[2], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", beta[3], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", beta[4], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", beta[5], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", beta[6], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", beta[7], "/ s.e.(b7) = ", varcovar[7,7]**.5

reg = pd.ols(y = data.dbirwt, x = data[['married','black','otherrace','dmeduc','dmage','dmage2','female']])
print "Pandas' built in OLS function gives me: "
print reg


################## Quantile Regression #####################

# have the guesses for beta be the OLS estimates
betaguess = beta
iterat = 1
funcVals = list()
varvals = np.zeros((1,193))

def QTreg_solve(betaguess, tau, Y, X):
    u = Y - (np.dot(X,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum() + np.abs(rho_neg.sum())

tau = .5
QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)
#beta_min = spicy.fmin(QTsolve,betaguess)

def callbackFunction(Xi):
    #global iterat
    global funcVals
    global varvals
    funcVals.append(QTsolve(Xi))
    varvals = np.vstack((varvals,Xi.reshape(1,193)))
    #print "Iteration ", iterat ,": f(x) = ", QTsolve(Xi) 
    #iterat += 1


t1 = time.time()
beta_min = spicy.minimize(QTsolve,betaguess,callback=callbackFunction, method='L-BFGS-B',tol=1e-7)
# beta_min = spicy.minimize(QTreg_solve,betaguess,args=(tau,Y,X),method = "Nelder-Mead")
totaltime = time.time()-t1
print beta_min
print "This took ", totaltime/60, " minutes."

print "Here are the function evaluations: "
print funcVals

# change back the output directory
"""
sys.stdout = orig_stdout
f.close()
"""

#now, plot some of the variable over the iterations

os.chdir("./VarVals")

plt.plot(range(len(funcVals)),funcVals)
plt.savefig('funcvals.png')

variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','state_4','year_2','year_3','year_4','state_time_2','state_time_3','state_time_4','state_time2_2','state_time2_3','state_time2_4']

k=0
for i in [0,1,2,3,4,5,6,7,8,9,10,58,59,60,93,94,95,143,144,145]: 
    plt.plot(range(len(varvals[:,i])),varvals[:,i])
    plt.savefig('varvals_'+variables[k]+'.png')
    plt.cla()
    k += 1


np.linalg.det(np.dot(Xsmall.T,Xsmall))
np.linalg.det(np.dot(X.T,X))


########################################################################
# Making a graph to see what is going on.
# The objective function as a funciton of one thing only, holding others constant


os.chdir("/Users/Alex/Economics/KoenkerHallock1991/objfunc_OLSest")

tau = .5
def QTobjectiveF(var, i):
    betaguess[i] = var
    return QTreg_solve(betaguess, tau, Y, X)


# First, graphing the objective function from the OLS estimates
k = 0
for i in [0,1,2,3,4,5,6,7,8,9,58,59,93,94,143,144]:
    betaguess = beta.copy()
    xval = betaguess[i].copy()

    m = 151 # grid size
    offs = 1
    of = 25
    grid = np.zeros(m)
    if i < 8:
        grid = np.linspace(betaguess[i]-offs*betaguess[i],betaguess[i]+offs*betaguess[i],m)
    if i >= 8:
        grid = np.linspace(betaguess[i]-of,betaguess[i]+of,m)
    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','year_2','year_3','state_time_2','state_time_3','state_time2_2','state_time2_3']
    plt.plot(grid,y)
    plt.plot(xval,y.min(),"ro")
    plt.title(variables[k])
    plt.savefig("objfuncfromOLSest_"+variables[k]+".png")
    plt.show()
    k += 1


# Now, graphing with the right estimates, but up close with big grid
os.chdir("/Users/Alex/Economics/KoenkerHallock1991/objfunc_QTbiggrid")

k = 0
for i in [0,1,2,3,4,5,6,7,8,9,10,58,59,60,93,94,95,143,144,145]: 
    betaguess = beta_min.x.copy()
    xval = betaguess[i].copy()

    m = 500 # grid size
    offs = .5
    of = 5
    grid = np.zeros(m)
    if i < 8:
        grid = np.linspace(betaguess[i]-offs*betaguess[i],betaguess[i]+offs*betaguess[i],m)
    if i >= 8:
        grid = np.linspace(betaguess[i]-of,betaguess[i]+of,m)
    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','state_4','year_2','year_3','year_4','state_time_2','state_time_3','state_time_4','state_time2_2','state_time2_3','state_time2_4']
    plt.plot(grid,y)
    plt.plot(xval,y.min(),"ro")
    plt.title(variables[k])
    plt.savefig("objfuncfromQTest_"+variables[k]+".png")
    plt.show()
    k += 1

xgrid = range(len(funcVals))
plt.plot(xgrid,funcVals)
plt.title('Objective function value')
plt.savefig('funcValue')
plt.show()











os.chdir("/Users/Alex/Economics/KoenkerHallock1991/objfuncstates")

k = 0
for i in range(20,58):
    betaguess = beta_min.x.copy()
    xval = betaguess[i].copy()

    m = 500 # grid size
    offs = .5
    of = 5
    grid = np.zeros(m)
    if i < 8:
        grid = np.linspace(betaguess[i]-offs*betaguess[i],betaguess[i]+offs*betaguess[i],m)
    if i >= 8:
        grid = np.linspace(betaguess[i]-of,betaguess[i]+of,m)
    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    #variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','state_4','year_2','year_3','year_4','state_time_2','state_time_3','state_time_4','state_time2_2','state_time2_3','state_time2_4']
    plt.plot(grid,y)
    plt.plot(xval,y.min(),"ro")
    plt.title("State " +str(i-6)+" Fixed FX")
    plt.savefig("objfuncfromQTest_state"+str(i-6)+".png")
    #plt.show()
    plt.cla()
    k += 1


################################################################################################
# Abadie, Angrist, Imbens (2002) approach to quantile instrumental variables
os.chdir("..")

Z = np.array(data.bldtest).reshape(n,1)


def AAIsolve(betaguess, tau, Y, X, Z, Wplus):
    # This first part is the normal quantile regression part (with the check function)
    u = Y - np.dot(X,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau
    rho_neg = u[u_neg]*(1-tau)
 
    # dividing the weights to go with the positive and negative values of u
    Wplus_pos = Wplus[u_pos]
    Wplus_neg = Wplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this

def AAIestimator(betaguess, tau, Y, X, Z):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator. Just put in the parameters, and it will solve for the coefficeints.
        It is currently not  generalized.
        Include the endogenous regressor (D) as the 2nd column in the matrix, X.
    """
    t1 = time.time()
    D = X[:,1].reshape(n,1)
    Xsmall = np.delete(X,1,1) # X without the endogenous regressor, D

    # This gets rid of the columns that are causing problems in the 1st stage probit regressions
    Xsmall2 = np.delete(Xsmall,91,1) # get rid of year 2004

    # This next part now takes care of the weights that are unique to the IV quantile
    # Probit reg Z on Xsmall, then get fitted values. This is Pr(Z=1|X), call them prZXfv
    pZX = sm.Probit(Z,Xsmall2).fit(disp=0)
    paramsZX = pZX.params
    prZXfv = sm.Probit(Z,Xsmall2).predict(paramsZX)
    # Probit reg Z on Y Xsmall D, then get fitted values. This is Pr(Z=1|Y,D,X), call them prZYDXfv
    YDX = np.concatenate((Y,D,Xsmall2),axis=1)
    pZYDX = sm.Probit(Z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    prZYDXfv = sm.Probit(Z,YDX).predict(paramsZYDX)

    D = D[:,0] # just to reshape D

    # Now here is the equation for the weights, as suggested
    fst = D*(1-prZYDXfv)/(1-prZXfv)
    fst[np.isnan(fst)] = 0
    snd = (1-D)*prZYDXfv/prZXfv
    snd[np.isnan(snd)] = 0
    W = 1 - fst - snd

    Wplus = np.array([np.zeros(len(W)),W]).max(axis=0) # This makes all negatives into zeros

    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, Y, X, Z, Wplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    
    t2 = time.time()
    tt = t1 - t2
    return betaIVQT, tt

#coeffs, t = AAIestimator(betaguess, tau, Y, X, Z)


# Graph of each coefficient by IV quantile
nq = 19
quantiles = np.linspace(.05,.95,nq)
coeffs = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

os.chdir("./coeffbyIVquantileAAI_fixedfx")

t1 = time.time()

for i in range(nq):
    tau = quantiles[i]
    betaIVQT, t = AAIestimator(betaguess, tau, Y, X, Z)
    coeffs[i] = betaIVQT.x
    success[i] = betaIVQT.success
    print "Done with quantile ", tau

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffs[:,i])
    plt.title("Coeff. of "+variables[i]+" by IV quantile")
    plt.savefig("AAIbyIVQuantile_"+variables[i]+"fixedfx.png")
    plt.cla()
    #plt.show()

print success

print "This all took, ", (t1 - time.time())/60, " hours"



