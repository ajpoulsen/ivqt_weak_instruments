# IVQTparallelSlave_aai.py

""" Quantile Instrumental Variables Regression in Parallel
Servant Processes 
(servants to 'IVQTparallel.py')


Written by Alex Poulsen, BYU """


import numpy as np
from numpy import linalg
import scipy.optimize as spicy
from pandas import Series, DataFrame, Index
import pandas as pd
import os
import time

import sys
from mpi4py import MPI

comm = MPI.Comm.Get_parent()
size = comm.Get_size()
rank = comm.Get_rank()

tau = np.array([0],'d')
comm.Bcast([tau,MPI.DOUBLE], root=0)
# Getting the data

#n = 88191783 # this is the amount of observations there are in our dataset.
n = 8847 # smaller dataset

local_n = n / size
remainder = n % size

if rank < remainder:
    local_n += 1

# Import the data (have each node import a different range of the dataset)
toskip = np.zeros(n-local_n-1)
if rank < remainder:
    toskip = range(1,local_n*rank+1) + range(local_n*(rank+1)+1, n+1)
if rank >= remainder:
    toskip = range(1,local_n*rank+remainder+1) + range(local_n*(rank+1)+remainder+1, n+1)

data = pd.io.parsers.read_table("marriagedata_forparallelSAMPLEsmall.txt",skiprows=toskip) # later, change this to the full dataset
# get rid of nanners
data = data.dropna()
data['cons'] = 1

# Converting all of the data fom pandas objects to numpy arrays
covars = np.array(data[['cons','black','otherrace','dmeduc','dmage','dmage2','female','dlivord']])

# Dummies
statedum = np.array(data.ix[:,'_Istate_2':'_Istate_56'])
yeardum = np.array(data.ix[:,'_Iyear_1970':'_Iyear_2004'])

# now putting them all together
X = np.concatenate((covars,statedum,yeardum), axis=1)
Z = np.array(data.bldtest).reshape(local_n,1)
Y = np.array(data.dbirwt).reshape(local_n ,1)
D = np.array(data.married).reshape(local_n ,1)
ZX = np.concatenate((Z,X),axis=1)

if rank==0: print 'Data imported'
##########################################################################

########### This one finds Y(1) ##############
######### First Stage: reg D on Z, X ######### 
cont = np.array([1],'i')
betaguess = np.zeros(94).astype('d')
while cont[0]:
    comm.Bcast([betaguess,MPI.DOUBLE], root=0)

    SSE = np.array(((D - np.dot(ZX,betaguess).reshape(local_n,1))**2).sum(),'d')

    comm.Reduce([SSE,MPI.DOUBLE],None, op=MPI.SUM, root = 0)
    comm.Bcast([cont,MPI.INT], root=0)
    comm.Barrier() # Barrier 1



if rank==0: print '1st Stage OLS regression done'


######### CDF Function ######### 
cont = np.array([1],'i')
betaguess = np.zeros(94).astype('d')
contCDF = np.array([1],'i')
yguess = np.array([0],'d')

while contCDF[0]:

    comm.Bcast([yguess,MPI.DOUBLE], root=0)
    # This calculates the reduced form coefficient
    while cont[0]: 
        comm.Bcast([betaguess,MPI.DOUBLE], root=0)

        W = 1*(Y<=yguess)*D
        SSE = np.array(((W - np.dot(ZX,betaguess).reshape(local_n,1))**2).sum(),'d')    

        comm.Reduce([SSE,MPI.DOUBLE],None, op=MPI.SUM, root = 0)
        comm.Bcast([cont,MPI.INT], root=0)
        comm.Barrier()
    #print rank
    comm.Barrier()
    comm.Bcast([contCDF,MPI.INT], root=0)
    

    #if rank==0: print 'CDF iteration'

'''
##########################################################################
# For Y(0)
D_0 = 1-D
######### First Stage: reg 1-D on Z, X ######### 
cont = np.array([1],'i')
betaguess = np.zeros(94).astype('d')
while cont[0]:
    comm.Bcast([betaguess,MPI.DOUBLE], root=0)

    SSE = np.array(((D_0 - np.dot(ZX,betaguess).reshape(local_n,1))**2).sum(),'d')

    comm.Reduce([SSE,MPI.DOUBLE],None, op=MPI.SUM, root = 0)
    comm.Bcast([cont,MPI.INT], root=0)

if rank==0: print '1st Stage OLS regression done'

######### CDF Function ######### 
cont = np.array([1],'i')
betaguess = np.zeros(94).astype('d')
while cont[0]:
    comm.Bcast([betaguess,MPI.DOUBLE], root=0)
    W = 1*(Y<=yguess)*D_0
    SSE = np.array(((W - np.dot(ZX,betaguess).reshape(local_n,1))**2).sum(),'d')    

    comm.Reduce([MLE,MPI.DOUBLE],None, op=MPI.SUM, root = 0)
    comm.Bcast([cont,MPI.INT], root=0)


'''

comm.Disconnect() 

