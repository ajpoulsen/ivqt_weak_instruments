# IVQTparallel_aai.py

""" Quantile Instrumental Variables Regression in Parallel
Master Process
to run this - 

mpiexec -n 1 python IVQTparallel.py


Written by Alex Poulsen, BYU """


import numpy as np
from numpy import linalg
import scipy.optimize as spicy
from pandas import Series, DataFrame, Index 
import pandas as pd 
import os 
import time

import sys
from mpi4py import MPI 


#os.chdir("/Users/poulsena/Documents/repos/marriage-and-infant-health/marriage_infant_health")

processes = 10
t1 = time.time()
comm = MPI.COMM_SELF.Spawn(sys.executable,args = ['IVQTparallelSlave_cdf.py'],maxprocs=processes)
tau = np.array([.5],'d') # Later I can make this a sys arg, and then run a shell script
comm.Bcast([tau,MPI.DOUBLE], root=MPI.ROOT)


# First stage: reg D on Z, X
def OLS(betaguess):
    comm.Bcast([betaguess,MPI.DOUBLE], root=MPI.ROOT)
    SSE = np.array([0],dtype='d')
    comm.Reduce(None,[SSE,MPI.DOUBLE], op=MPI.SUM, root = MPI.ROOT)
    comm.Bcast([np.array([1],'i'),MPI.INT], root=MPI.ROOT)
    comm.Barrier()
    return SSE


# Here is the CDF function.
def CDF(yguess, delta_FS, tau):
    # Calculate W(y) in the slave process
    # Solving the Reduced form after every iteration: reg W(y) on Z, X
    comm.Bcast([yguess,MPI.DOUBLE], root=MPI.ROOT)
    betaguess = np.zeros(94).astype('d')
    ###########
    # This calculates the reduced form coefficient
    coeffs_RF = spicy.minimize(OLS,betaguess,method='Powell')
    # This little block is to get the slave processes to stop
    comm.Bcast([betaguess,MPI.DOUBLE], root=MPI.ROOT)
    SSE = np.array([0],dtype='d')
    comm.Reduce(None,[SSE,MPI.DOUBLE], op=MPI.SUM, root = MPI.ROOT)
    cont = np.array([0],'i')
    comm.Bcast([cont,MPI.INT], root=MPI.ROOT)
    comm.Barrier()
    ###########
    comm.Barrier()
    contCDF = np.array([1],'i')
    comm.Bcast([contCDF,MPI.INT], root=MPI.ROOT) # This is to keep the outer while loop going
    

    delta_RF = coeffs_RF.x[1]

    return abs(delta_RF/delta_FS - tau)

########### This one finds Y(1) ##############

betaguess = np.zeros(94).astype('d')

######### First Stage: reg D on Z, X ######### 
t1 = time.time()
coeffs_FS = spicy.minimize(OLS,betaguess,method='Powell')
t2 = time.time()

print "Powell took: ", t2-t1



# This little block is to get the slave processes to stop
comm.Bcast([betaguess,MPI.DOUBLE], root=MPI.ROOT)
SSE = np.array([0],dtype='d')
comm.Reduce(None,[SSE,MPI.DOUBLE], op=MPI.SUM, root = MPI.ROOT)
cont = np.array([0],'i')
comm.Bcast([cont,MPI.INT], root=MPI.ROOT)

comm.Barrier() # Barrier 1

delta_FS = coeffs_FS.x[1]


######### CDF Function ######### 
yguess = np.array([3340],'d')
CDF1 = lambda yguess: CDF(yguess, delta_FS, tau)
print '1st iter:',CDF1(yguess)
print '2nd iter:',CDF1(yguess)
print '3rd iter:',CDF1(yguess)

# This is to get the double while loop of the CDF function to stop
comm.Bcast([yguess,MPI.DOUBLE], root=MPI.ROOT)
# Inner
comm.Bcast([betaguess,MPI.DOUBLE], root=MPI.ROOT)
SSE = np.array([0],dtype='d')
comm.Reduce(None,[SSE,MPI.DOUBLE], op=MPI.SUM, root = MPI.ROOT)
cont = np.array([0],'i')
comm.Bcast([cont,MPI.INT], root=MPI.ROOT)
comm.Barrier()
comm.Barrier()
contCDF = np.array([0],'i') # this tells the outer while loop to stop
comm.Bcast([contCDF,MPI.INT], root=MPI.ROOT)



"""
y_minned_1 = spicy.minimize(CDF1,yguess,method='Powell')

# This is to get the double while loop of the CDF function to stop
comm.Bcast([yguess,MPI.DOUBLE], root=MPI.ROOT)
# Inner
comm.Bcast([betaguess,MPI.DOUBLE], root=MPI.ROOT)
SSE = np.array([0],dtype='d')
comm.Reduce(None,[SSE,MPI.DOUBLE], op=MPI.SUM, root = MPI.ROOT)
cont = np.array([0],'i')
comm.Bcast([cont,MPI.INT], root=MPI.ROOT)

contCDF = np.array([0],'i') # this tells the outer while loop to stop
comm.Bcast([contCDF,MPI.INT], root=MPI.ROOT)




print 'Finished Y(1)'

########### This one finds Y(0) ##############
coeffs_FS = spicy.minimize(OLS,betaguess,method='Powell')

# This little block is to get the slave processes to stop
SSE = np.array([0],dtype='d')
comm.Bcast([betaguess,MPI.DOUBLE], root=MPI.ROOT)
comm.Reduce(None,[SSE,MPI.DOUBLE], op=MPI.SUM, root = MPI.ROOT)
cont = np.array([0],'i')
comm.Bcast([cont,MPI.INT], root=MPI.ROOT)

delta_FS = coeffs_FS.x[1]

CDF0 = lambda yguess: CDF(yguess, delta_FS, tau)
y_minned_0 = spicy.minimize(CDF0,yguess,method='Powell')



# Final Results

treatment_effect = y_minned_1.x - y_minned_0.x

print treatment_effect


t2 = time.time()
print 'This took ', t2-t1, ' in parallel.'
"""
comm.Disconnect()



