#montecarlo_se.py


import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model
import statsmodels.api as smm
from matplotlib import pyplot as plt
from numpy import linalg
import scipy.optimize as spicy
import os 

# /Users/poulsena/Documents/ivqt_weak_instruments



### Quantile Instrumental Variables Regression (Chernozhukov & Hansen)
def QTreg_solve(betaguess, tau, y, d):
	d = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	u = y - (np.dot(d,betaguess)).reshape(n,1)
	# here is the tilted absolute value function
	u_pos = np.where(u>0)[0]
	u_neg = np.where(u<=0)[0]
	rho_pos = u[u_pos]*tau 
	rho_neg = u[u_neg]*(1-tau)
	return rho_pos.sum() + np.abs(rho_neg.sum())

def K(x):
    """ Standard Normal """
    return np.exp((-1)*x**2/2)/(2*np.pi)**.5


def IVQTreg(betaguess, tau, y, d, z, gridlen,graphs = False, var=False):
    """This function is based off of Chernozhukov & Hansen (2003)."""

    alpha = np.linspace(50-.25*y.var()**.5,50+.25*y.var()**.5,gridlen) # 50 b/c that is the truth
    V = y - d*alpha # creates a matrix (n x gridlen)
    #regressor = np.concatenate((d,z),axis=1)
    betas = np.zeros((gridlen,z.shape[1]+1))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), z)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell')
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,0]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)

    # Estimating Variance/ Confidence Intervals
    std_err = 0
    if var == True:
	    k = 2
	    D_cons = np.concatenate((np.ones(n).reshape(n,1),d),axis=1)
	    eps_hat = y - np.dot(D_cons,rightcoeffs[1:]).reshape(n,1)
	    psi = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
	    s = (tau - 1*(eps_hat < 0))* psi
	    S = np.dot(s.T,s)*(1.0/n)
	    S = np.zeros((k,k))
	    for i in range(n):
	        s_i = s[i].reshape(k,1)
	        S += np.dot(s_i,s_i.T)
	    S *= (1.0/n)

	    sigma_hat = (eps_hat.var())**.5
	    h = (4*sigma_hat**5/(3*n))**(1.0/5)

	    DX = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
	    J = np.zeros((k,k))
	    for i in range(n):
	        psi_i = psi[i].reshape(k,1)
	        DX_i = DX[i].reshape(1,k)
	        J += np.dot(psi_i,DX_i)* K(eps_hat[i]/h)*(1/h)
	    J *= (1.0/n)

	    omega = (1.0/n)*np.dot(np.dot(np.linalg.inv(J),S),np.linalg.inv(J.T))
	    std_err =  np.diagonal(omega**.5)
	    c = 1.96 # from the T-table
	    # Confidence Intervals
	    upper = rightalpha + c*std_err[0]
	    lower = rightalpha - c*std_err[0]
    #plot this function to make sure our min is an interior point
    if graphs == True:
        plt.plot(alpha,abs(betas[:,0]))
        plt.plot(rightalpha,abs(betas[rightalpha_i][0]),'ro')
        plt.axvspan(lower, upper, facecolor='#00cccc')
        plt.ylabel('Coefficient of Z')
        plt.xlabel('Coefficient on D')
        plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
        plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
        plt.cla()
        #plt.show()
    #print "Done with quantile ", tau
    return rightcoeffs, std_err, success.sum() == gridlen

# Making Data
n = 10000

coeffs_MC = np.zeros((9,3,100))

for j in range(100):

	eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
	eps_d = (np.random.standard_normal(n)*100).reshape(n,1)
	eps_y = (np.random.standard_normal(n)*100).reshape(n,1)
	strength = .6
	z = 1*(eps_z > 0).reshape(n,1)
	d = 1 * (strength*eps_z + eps_d + eps_y > 0).reshape(n,1)
	y = (1 + 50*d + eps_y).reshape(n,1)

	betaguess = np.array([1,1])
	gridlen = 100
	quantiles = np.linspace(1,9,num=9)
	nq = len(quantiles)

	k = len(betaguess)
	coeffs_CH = np.zeros((nq,k+1)) #(z_minned,constant,d )
	std_errs_CH = np.zeros((nq,k))
	successIVQ = np.zeros((nq,1))

	for i in range(nq):
	    tau = quantiles[i]*.1
	    ivqreg = IVQTreg(betaguess, tau, y, d, z, gridlen)
	    coeffs_CH[i] = ivqreg[0]
	    std_errs_CH[i] = ivqreg[1]
	    successIVQ[i] = ivqreg[2]

	coeffs_MC[:,:,j] = coeffs_CH
	print "Done with simulation ", j+1

std_dev_d  = (coeffs_MC[:,2,:].var(axis=1))**.5

# Looks like the bootstrapped s.e. are right




In [23]: (coeffs_MC[:,2,:].var(axis=1))**.5
Out[23]: 
array([ 19.79355016,  19.45754077,  19.60024422,  19.55498066,
        15.81211635,  19.27110718,  18.26023387,  18.41767874,  17.70237433])



