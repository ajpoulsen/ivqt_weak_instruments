
import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model

import statsmodels.api as sm

# Making Data
n = 10000

eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
eps_d = (np.random.standard_normal(n)*100).reshape(n,1)
eps_y = (np.random.standard_normal(n)*100).reshape(n,1)

z = 1*(eps_z > 0).reshape(n,1)
d = 1 * (.3*eps_z + eps_d + eps_y > 0).reshape(n,1)
y = (1 + 50*d + eps_y).reshape(n,1)


# First Stage
FS = sm.OLS(d, exog=sm.add_constant(z))
fs_fit = FS.fit()
print fs_fit.params
print fs_fit.bse

# Make sure the FS is significant
print fs_fit.params[1]/fs_fit.bse[1]

# 2SLS
IV = gmm.IV2SLS(y, d, instrument=z)
iv_fit = IV.fit()
print iv_fit.params


