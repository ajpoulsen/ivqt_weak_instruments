
import numpy as np 
import statsmodels.sandbox.regression.gmm as gmm
import statsmodels.regression.linear_model
import statsmodels.api as smm
from matplotlib import pyplot as plt
from numpy import linalg
import scipy.optimize as spicy
import os 
import statsmodels.discrete.discrete_model as sm

# /Users/poulsena/Documents/ivqt_weak_instruments



def K(x):
    """ Standard Normal Kernel"""
    return np.exp((-1)*x**2/2)/(2*np.pi)**.5

################################################################################################
####  Quantile Instrumental Variables Regression (Chernozhukov & Hansen)
def QTreg_solve(betaguess, tau, y, d):
    d = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    u = y - (np.dot(d,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    return rho_pos.sum() + np.abs(rho_neg.sum())


def CH_point_est(betaguess, tau, y, d, z, gridlen):
    """This function is based off of Chernozhukov & Hansen (2003)."""

    alpha = np.linspace(100-.5*y.var()**.5,100+.5*y.var()**.5,gridlen) # 100 b/c that is the truth
    V = y - d*alpha # creates a matrix (n x gridlen)
    #regressor = np.concatenate((d,z),axis=1)
    betas = np.zeros((gridlen,z.shape[1]+1))
    success = np.zeros(gridlen)
    for j in range(gridlen):
        QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, V[:,j].reshape(n,1), z)
        betaQT = spicy.minimize(QTsolve,betaguess,method='Powell')
        betas[j] = betaQT.x
        success[j] = betaQT.success
    rightalpha_i = np.argmin(abs(betas[:,0]))
    rightcoeffs = betas[rightalpha_i]
    rightalpha = alpha[rightalpha_i]
    rightcoeffs = np.concatenate((rightcoeffs,np.array([rightalpha])),axis=1)
    rightcoeffs = rightcoeffs[::-1]
    return rightcoeffs
    
def IVQT_CH(betaguess, tau, y, d, z, gridlen=100,graphs = False,boots=False):
    """This function is based off of Chernozhukov & Hansen (2003)."""    

    rightcoeffs = CH_point_est(betaguess, tau, y, d, z, gridlen)
    # Estimating Variance/ Confidence Intervals
    k = 2
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    eps_hat = y - np.dot(D_cons,rightcoeffs[:2]).reshape(n,1)
    psi = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
    s = (tau - 1*(eps_hat < 0))* psi
    S = np.dot(s.T,s)*(1.0/n)
    S = np.zeros((k,k))
    for i in range(n):
        s_i = s[i].reshape(k,1)
        S += np.dot(s_i,s_i.T)
    S *= (1.0/n)

    sigma_hat = (eps_hat.var())**.5
    h = (4*sigma_hat**5/(3*n))**(1.0/5)

    DX = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    J = np.zeros((k,k))
    for i in range(n):
        psi_i = psi[i].reshape(k,1)
        DX_i = DX[i].reshape(1,k)
        J += np.dot(psi_i,DX_i)* K(eps_hat[i]/h)*(1/h)
    J *= (1.0/n)

    omega = (1.0/n)*np.dot(np.dot(np.linalg.inv(J),S),np.linalg.inv(J.T))
    std_err_CF =  np.diagonal(omega**.5)
    c = 1.96 # from the T-table
    # Confidence Intervals
    upper = rightcoeffs[0] + c*std_err_CF[0]
    lower = rightcoeffs[0] - c*std_err_CF[0]
    #plot this function to make sure our min is an interior point
    if graphs == True:
        plt.plot(alpha,abs(betas[:,0]))
        plt.plot(rightalpha,abs(betas[rightalpha_i][0]),'ro')
        plt.axvspan(lower, upper, facecolor='#00cccc')
        plt.ylabel('Coefficient of Z')
        plt.xlabel('Coefficient on D')
        plt.title('Minimizing the coefficient on Z of quantile ' + str(tau))
        #plt.savefig('MinZ_' + str(tau*100)[:-2] +'pc.png'  )
        #plt.cla()
        plt.show()
    #print "Done with quantile ", tau
    std_err_boot = False
    if boots == True:
        # Lets do some bootstraps
        bootsnum = 100
        bootstraps = np.zeros(bootsnum)
        data = np.concatenate((y,d,z),axis=1)

        for j in range(bootsnum):
            newdata = data[np.random.randint(0, len(data), size=len(data))]
            newy,newd,newz = newdata[:,0].reshape(n,1),newdata[:,1].reshape(n,1),newdata[:,2].reshape(n,1)
            # done resample, now each quantile reg
            coeffs = CH_point_est(betaguess, tau, newy, newd, newz,gridlen)[0]

            bootstraps[j] = coeffs

        std_err_boot = ((bootstraps.var(axis=0))**.5).T

    return rightcoeffs, std_err_CF, std_err_boot


os.chdir("/Users/poulsena/Documents/ivqt_weak_instruments/C_H/biggrid")


################################################################################################
# Abadie, Angrist, Imbens (2002) approach to quantile instrumental variables
os.chdir("..")



def AAIsolve(betaguess, tau, y, d, z, kappaplus):
    D = (d.copy()).reshape(n,1)
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = kappaplus[u_pos]
    Wplus_neg = kappaplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def AAI_point_est(betaguess, tau, y, d, z):

    D = d.copy()
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)"""
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    #YDX = np.concatenate((y,D_cons,y**2,y**3,y*D,(y*D)**2 ),axis=1)
    YDX = np.concatenate((y,D_cons,y*D,y**2,y**2*D,y**3,y**3*D),axis=1)

    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    #negW = 1.0*(kappa < 0).sum() / len(kappa)
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z, kappaplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    return betaIVQT.x, kappaplus



# THIS IS THE RIGHT ONE!
def IVQT_AAI(betaguess, tau, y, d, z, boots=False):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    betaIVQT,kappaplus = AAI_point_est(betaguess, tau, y, d, z)
    prZ = z.mean()
    """
    # Standard Errors
    n_ = len(np.where(kappaplus>0)[0])
    k = 2
    W_mat = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    eps_hat = y - np.dot(W_mat,betaIVQT).reshape(n,1)
    sigma_hat = (eps_hat.var())**.5
    h = (4*sigma_hat**5/(3*n))**(1.0/5)

    ugly = ((1-d)*z/prZ**2) - (d*(1-z)/(1-prZ)**2)

    H = np.zeros((k,k))
    for i in range(n):
        W_i = W_mat[i].reshape(k,1)
        H += (tau - 1*(eps_hat[i]<0)) * W_i * ugly[i] 
    H *= (1./n_)

    J = np.zeros((k,k))
    for i in range(n):
        W_i = W_mat[i].reshape(k,1)
        J += kappaplus[i]* (1/h)*K(eps_hat[i,0]/h)* np.dot(W_i,W_i.T)
    J *= (1./n_)

    kappa_V = 1 - d*(1-z)/prZ - (1-d)*z/prZ

    Sigma = np.zeros((k,k))
    for i in range(n):
        W_i = W_mat[i].reshape(k,1)
        psi = kappa_V[i] * (tau - 1*(eps_hat[0]<0)) * W_i + H*(z[i] - prZ)
        Sigma += np.dot(psi,psi.T)
    Sigma *= (1./n_)

    J_inv = np.linalg.inv(J)
    omega = (1./n_)*np.dot(np.dot(J_inv,Sigma),J_inv)
    std_err_CF =  np.diagonal(omega**.5)
    """
    std_err_CF = np.zeros(2)
    std_err_boot = False
    if boots == True:
        # Lets do some bootstraps
        bootsnum = 100
        bootstraps = np.zeros(bootsnum)
        data = np.concatenate((y,d,z),axis=1)

        for j in range(bootsnum):
            newdata = data[np.random.randint(0, len(data), size=len(data))]
            newy,newd,newz = newdata[:,0].reshape(n,1),newdata[:,1].reshape(n,1),newdata[:,2].reshape(n,1)
            # done resample, now each quantile reg
            coeffs = AAI_point_est(betaguess, tau, newy, newd, newz)[0][0]

            bootstraps[j] = coeffs

        std_err_boot = ((bootstraps.var(axis=0))**.5).T

    return betaIVQT, std_err_CF, std_err_boot

################################################################################################
# The Inverse CDF approach to quantile instrumental variables (Frandsen)

def CDF_func(yguess, X, XX, y, d,delta_FS,tau):
    W = (y <= yguess)*d

    XW = np.dot(X.T,W)
    b_fs = np.linalg.solve(XX,XW)
    delta_RF = b_fs[0,0]

    beta_2sls = delta_RF / delta_FS

    diff = beta_2sls - tau
    return diff


def point_est_CDF(tau, y, d, z):
    #********* This finds Y(1) ************
    # First stage regression
    X = np.concatenate((z,np.ones(n).reshape(n,1)),axis=1)
    XX = np.dot(X.T,X)
    XD = np.dot(X.T,d)
    b_fs = np.linalg.solve(XX,XD)
    delta_FS = b_fs[0,0]
    lo = y.min()
    up = y.max()

    y_1 = spicy.brentq(CDF_func,lo,up,args=(X, XX, y, d, delta_FS, tau))
    # Maybe experiment withy changing tolerance in this function

    #********* This finds Y(0) ************
    D_0 = 1 - d
    # First stage regression
    XD_0 = np.dot(X.T,D_0)
    b_fs = np.linalg.solve(XX,XD_0)
    delta_FS = b_fs[0,0]

    y_0 = spicy.brentq(CDF_func,lo,up,args=(X, XX, y, D_0, delta_FS, tau))

    treatment = y_1-y_0
    return treatment


def IVQT_CDF(tau, y, d, z, bootstraps = 100):

    point_est = point_est_CDF(tau, y, d, z)
    # Lets do some bootstraps
    bootsnum = 100
    bootstraps = np.zeros(bootsnum)
    data = np.concatenate((y,d,z),axis=1)

    for j in range(bootsnum):
        newdata = data[np.random.randint(0, len(data), size=len(data))]
        newy,newd,newz = newdata[:,0].reshape(n,1),newdata[:,1].reshape(n,1),newdata[:,2].reshape(n,1)
        # done resample, now each quantile reg
        coeffs = point_est_CDF(tau, newy, newd, newz)

        bootstraps[j] = coeffs

    std_err_boot = ((bootstraps.var(axis=0))**.5).T

    return point_est, std_err_boot

################################################################################################

betaguess = np.array([0,0])
gridlen = 150
tau = .9
# Monte Carlo Tests
n = 500
mc = 2000

strengths = [15, 10, 6]
#The matrices of results
biases = np.zeros((3,3))
MSEs = np.zeros((3,3))
Vars = np.zeros((3,3))
coverages = np.zeros((3,3))
avg_FSs = np.zeros((3,1))

j = 0
for gamma in strengths:
    t_stats = np.zeros(mc)
    coeffs = np.zeros((mc,3)) #(CH, AAI, CDF)
    st_errs = np.zeros((mc,3))
    st_errs_B = np.zeros((mc,3))


    for i in range(mc):
        #strength = 20
        coeff = 100
        #while t_stats[i] < 2.1:
        eps_z = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_d = (np.random.standard_normal(n)*10).reshape(n,1)
        eps_y = (np.random.standard_normal(n)*10).reshape(n,1)
        z = 1*(eps_z > 0).reshape(n,1)
        d = 1 * (gamma*z + eps_d + eps_y > 0).reshape(n,1)
        y = (1 + coeff*d + eps_y).reshape(n,1)

        # First Stage
        FS = smm.OLS(d, exog=smm.add_constant(z))
        fs_fit = FS.fit()
        # Make sure the FS is significant
        t_stats[i] = fs_fit.params[1]/fs_fit.bse[1]
        #print t_stats[i]
        print i
        ###########################################
        CH = IVQT_CH(betaguess, tau, y, d, z, gridlen)
        AAI = IVQT_AAI(betaguess, tau, y, d, z)

        max_tries = 150
        tries = 0
        while tries < max_tries:
            try:
                CDF = IVQT_CDF(tau, y, d, z, bootstraps = 100)
            except:
                tries +=1
                print "Trying again! ", tries
                CDF = np.array([float('NaN'),float('NaN')])
                if tries == max_tries: print "PROBLEM!! No convergence for CDF."
            else:
                tries = max_tries

        coeffs[i,0], st_errs[i,0] = AAI[0][0], AAI[1][0]
        coeffs[i,1], st_errs[i,1] = CH[0][0], CH[1][0] 
        coeffs[i,2], st_errs[i,2] = CDF[0], CDF[1]


    avg_FS = t_stats.mean()
    bias =  coeffs.mean(axis=0) - 100
    SQ_ERR = (coeffs - 100)**2
    MSE = SQ_ERR.mean(axis=0)
    Var = MSE - bias**2

    coverage_right = (coeffs < coeff + 1.96*st_errs)
    coverage_left  = (coeffs > coeff - 1.96*st_errs)
    coverage = (coverage_left*coverage_right).mean(axis=0) # This should be about 95%

    biases[j,:] = bias
    MSEs[j,:] = MSE
    Vars[j,:] = Var
    coverages[j,:] = coverage
    avg_FSs[j,0] = avg_FS

    j += 1

    print bias
    print MSE
    print Var
    print avg_FS
    print coverage


print "tau = ", tau
print "n = ", n
"""
print biases
print MSEs
print Vars
print avg_FSs
print coverages
"""
gammas = np.array(strengths).reshape(3,1)
np.set_printoptions(suppress=True)
table = np.concatenate((gammas,avg_FSs,biases,MSEs,coverages),axis=1)
table = np.around(table,decimals=4)
print table



print "\\begin{table}[ht] "
print "\caption{With $\\tau=",tau,"$ and $n=",n, "$}" 
print """
    \centering 
    \scalebox{.9}{

    \\begin{tabular}{l l | r r r | r r r | r r r} 
    \multicolumn{2}{c}{} & \multicolumn{3}{c}{Bias} & \multicolumn{3}{c}{MSE}& \multicolumn{3}{c}{Coverage}\\\\

    \hline\hline %inserts double horizontal lines
    $\gamma$ & T-Stat & AAI & CH & ICE & AAI & CH & ICE & AAI & CH & ICE \\\\ [0.5ex]  
    
    \hline % inserts single horizontal line    """
print table[0,0] , "&" , table[0,1] , "&" , table[0,2] , "&" , table[0,3] , "&" , table[0,4] , "&" , table[0,5] , "&" , table[0,6] , "&" , table[0,7] , "&" , "\\textemdash" , "&" , table[0,9] ,  "&" , table[0,10] , " \\\ "
print table[1,0] , "&" , table[1,1] , "&" , table[1,2] , "&" , table[1,3] , "&" , table[1,4] , "&" , table[1,5] , "&" , table[1,6] , "&" , table[1,7] , "&" , "\\textemdash" , "&" , table[1,9] ,  "&" , table[1,10] , " \\\ "
print table[2,0] , "&" , table[2,1] , "&" , table[2,2] , "&" , table[2,3] , "&" , table[2,4] , "&" , table[2,5] , "&" , table[2,6] , "&" , table[2,7] , "&" , "\\textemdash" , "&" , table[2,9] ,  "&" , table[2,10] , " \\\ [1ex] "
print """
    \hline 
    \end{tabular} 
    }
    \label{table:nonlin} 
    \end{table} 
"""





