
def AAIsolve(betaguess, tau, y, d, z, kappaplus):
    D = (d.copy()).reshape(n,1)
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = kappaplus[u_pos]
    Wplus_neg = kappaplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def AAI_point_est(betaguess, tau, y, d, z):

    D = d.copy()
    D_cons = np.concatenate((D,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)"""
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*D,(y*D)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = D[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    negW = 1.0*(kappa < 0).sum() / len(kappa)
    print negW
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z, kappaplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    return betaIVQT.x, kappaplus



# THIS IS THE RIGHT ONE!
def IVQT_AAI(betaguess, tau, y, d, z, boots=False):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    betaIVQT,kappaplus = AAI_point_est(betaguess, tau, y, d, z)
    prZ = z.mean()

    # Standard Errors
    n_ = len(np.where(kappaplus>0)[0])
    k = 2
    W_mat = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    eps_hat = y - np.dot(W_mat,betaIVQT).reshape(n,1)
    sigma_hat = (eps_hat.var())**.5
    h = (4*sigma_hat**5/(3*n))**(1.0/5)

    ugly = ((1-d)*z/prZ**2) - (d*(1-z)/(1-prZ)**2)

    H = np.zeros((k,k))
    for i in range(n):
        W_i = W_mat[i].reshape(k,1)
        H += (tau - 1*(eps_hat[i]<0)) * W_i * ugly[i] 
    H *= (1./n_)

    J = np.zeros((k,k))
    for i in range(n):
        W_i = W_mat[i].reshape(k,1)
        J += kappaplus[i]* (1/h)*K(eps_hat[i,0]/h)* np.dot(W_i,W_i.T)
    J *= (1./n_)

    kappa_V = 1 - d*(1-z)/prZ - (1-d)*z/prZ

    Sigma = np.zeros((k,k))
    for i in range(n):
        W_i = W_mat[i].reshape(k,1)
        psi = kappa_V[i] * (tau - 1*(eps_hat[0]<0)) * W_i + H*(z[i] - prZ)
        Sigma += np.dot(psi,psi.T)
    Sigma *= (1./n_)

    J_inv = np.linalg.inv(J)
    omega = (1./n_)*np.dot(np.dot(J_inv,Sigma),J_inv)
    std_err_CF =  np.diagonal(omega**.5)


    std_err_boot = False
    if boots == True:
        # Lets do some bootstraps
        bootsnum = 100
        bootstraps = np.zeros(bootsnum)
        data = np.concatenate((y,d,z),axis=1)

        for j in range(bootsnum):
            newdata = data[np.random.randint(0, len(data), size=len(data))]
            newy,newd,newz = newdata[:,0].reshape(n,1),newdata[:,1].reshape(n,1),newdata[:,2].reshape(n,1)
            # done resample, now each quantile reg
            coeffs = AAI_point_est(betaguess, tau, newy, newd, newz)[0][0]

            bootstraps[j] = coeffs

        std_err_boot = ((bootstraps.var(axis=0))**.5).T

    return betaIVQT, std_err_CF, std_err_boot


###########################################################################################


def AAIsolve(betaguess, tau, y, d, z, kappaplus):
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = kappaplus[u_pos]
    Wplus_neg = kappaplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def AAI_point_est(betaguess, tau, y, d, z):
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)"""
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*d,(y*d)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = d[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    #negW = 1.0*(kappa < 0).sum() / len(kappa)
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z, kappaplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    return betaIVQT.x


def IVQT_AAI(betaguess, tau, y, d, z, boots=False):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    betaIVQT = AAI_point_est(betaguess, tau, y, d, z)
    prZ = z.mean()

    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)"""
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*d,(y*d)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = d[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0)



    # Standard Errors
    u_hat = y - np.dot(D_cons,betaIVQT).reshape(n,1)
    h = (4*((u_hat.var())**(.5))**5/(3*n))**(1.0/5)

    J = np.zeros((2,2))

    for i in range(n):
        x_i = D_cons[i,:].reshape(1,2)
        J += np.dot(x_i.T,x_i)* (abs(u_hat[i]) <= h)
    J *= (2*n*h)**(-1)

    Sigma = tau*(1-tau)* np.dot(D_cons.T,D_cons)*(1./n)

    J_inv = np.linalg.inv(J)
    omega = np.dot(np.dot(J_inv,Sigma),J_inv)*(1./n)

    std_err_CF =  np.diagonal(omega**.5)


    std_err_boot = False
    if boots == True:
        # Lets do some bootstraps
        bootsnum = 100
        bootstraps = np.zeros(bootsnum)
        data = np.concatenate((y,d,z),axis=1)

        for j in range(bootsnum):
            newdata = data[np.random.randint(0, len(data), size=len(data))]
            newy,newd,newz = newdata[:,0].reshape(n,1),newdata[:,1].reshape(n,1),newdata[:,2].reshape(n,1)
            # done resample, now each quantile reg
            coeffs = AAI_point_est(betaguess, tau, newy, newd, newz)[0][0]

            bootstraps[j] = coeffs

        std_err_boot = ((bootstraps.var(axis=0))**.5).T

    return betaIVQT, std_err_CF, std_err_boot



########################################################

# With regular quantile s.e. ---- For exogenous treatment

def AAIsolve(betaguess, tau, y, d, z, kappaplus):
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = kappaplus[u_pos]
    Wplus_neg = kappaplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def AAI_point_est(betaguess, tau, y, d, z):
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*d,(y*d)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)
    """

    D = d[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    Z = z[:,0]
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-Z)/(1-Z) - (1-D)*Z/Z
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    #negW = 1.0*(kappa < 0).sum() / len(kappa)
    kappaplus = np.ones(n)
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z, kappaplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    return betaIVQT.x


def IVQT_AAI(betaguess, tau, y, d, z, boots=False):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    betaIVQT = AAI_point_est(betaguess, tau, y, d, z)
    prZ = z.mean()

    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*d,(y*d)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = d[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0)
    """
    Z = z[:,0]
    D = d[:,0]
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-Z)/(1-Z) - (1-D)*Z/Z

    # Standard Errors
    eps_hat = y - np.dot(D_cons,betaIVQT).reshape(n,1)
    h = (4*((eps_hat.var())**(.5))**5/(3*n))**(1.0/5)
    #h = 5*n**(-1./3)
    J = np.zeros((2,2))

    for i in range(n):
        x_i = D_cons[i,:].reshape(1,2)
        J += np.dot(x_i.T,x_i)* (abs(eps_hat[i]) <= h)
    J *= (2*n*h)**(-1)

    Sigma = tau*(1-tau)* np.dot(D_cons.T,D_cons)*(1./n)

    J_inv = np.linalg.inv(J)
    omega = np.dot(np.dot(J_inv,Sigma),J_inv)*(1./n)

    std_err_CF =  np.diagonal(omega**.5)

    return betaIVQT, std_err_CF

########################################################

# Different way to estimate s.e.


def AAIsolve(betaguess, tau, y, d, z, kappaplus):
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This first part is the normal quantile regression part (with the check function)
    u = y - np.dot(D_cons,betaguess).reshape(n,1)
    u_pos = np.where(u>0)[0] # dividing them up into positive and negative
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)
    
    Wplus_pos = kappaplus[u_pos]
    Wplus_neg = kappaplus[u_neg]

    argmin_this = np.dot(Wplus_pos,rho_pos[:,0]) + abs(np.dot(Wplus_neg,rho_neg[:,0]))

    return argmin_this


def AAI_point_est(betaguess, tau, y, d, z):
    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)"""
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*d,(y*d)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = d[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0) # This makes all negatives into zeros
    # dividing the weights to go with the positive and negative values of u
    #negW = 1.0*(kappa < 0).sum() / len(kappa)
    AAIsolver = lambda betaguess: AAIsolve(betaguess, tau, y, d, z, kappaplus)
    betaIVQT = spicy.minimize(AAIsolver,betaguess,method='Powell')
    return betaIVQT.x


def IVQT_AAI(betaguess, tau, y, d, z, boots=False):
    """ This function is to solve for the Abadie, Angrist, Imbens (2002) Quantile IV
        estimator.
    """
    betaIVQT = AAI_point_est(betaguess, tau, y, d, z)
    prZ = z.mean()

    D_cons = np.concatenate((d,np.ones(n).reshape(n,1)),axis=1)
    # This part takes care of the weights
    """# This is Pr(z=1|X), call them prZ
    X = np.ones(n).reshape(n,1)
    pZX = sm.Probit(z,X).fit(disp=0)
    paramsZX = pZX.params
    prZ = sm.Probit(z,X).predict(paramsZX)"""
    prZ = z.mean()

    # Probit reg Z on Y X D, then get fitted values. This is Pr(Z=1|Y,D,X), call them fvZ
    YDX = np.concatenate((y,D_cons,y**2,y**3,y*d,(y*d)**2 ),axis=1)
    pZYDX = sm.Probit(z,YDX).fit(disp=0)
    paramsZYDX = pZYDX.params
    fvZ = sm.Probit(z,YDX).predict(paramsZYDX)


    D = d[:,0] # This just changes the shape of D to conform with fvZ and PrZ
    # Now here is the equation for the weights, as suggested
    kappa = 1 - D*(1-fvZ)/(1-prZ) - (1-D)*fvZ/prZ
    kappaplus = np.array([np.zeros(len(kappa)),kappa]).max(axis=0)

    kappa_v = 1 - D*(1-z[0])/(1-prZ) - (1-D)*z[0]/prZ

    # Standard Errors
    k = 2
    u_hat = y - np.dot(D_cons,betaIVQT).reshape(n,1)
    h = (4*((u_hat.var())**(.5))**5/(3*n))**(1.0/5)

    J = np.zeros((k,k))

    for i in range(n):
        W_i = D_cons[i,:].reshape(1,k)
        J += np.dot(W_i.T,W_i) * kappaplus[i] * (1/h)*K(eps_hat[i,0]/h) #* (abs(u_hat[i]) <= h)
    J *= (2*n*h)**(-1)

    #Sigma = tau*(1-tau)* np.dot(D_cons.T,D_cons)*(1./n)

    M = np.zeros((n,k))
    H = np.zeros((n,k))

    for i in range(n):
        W_i = D_cons[i,:].reshape(1,k)
        M[i] = (tau - (u_hat[i] <0)) * W_i 
        H[i] = M[i] * (-d[i]*(1-z[i])/(1-prZ)**2 + -z[i]*(1-d[i])/(prZ)**2)

    H = (H.mean(axis = 0))#.reshape((k,1))

    psi = np.zeros((n,k))
    psipsi = np.zeros((k,k,n))
    for i in range(n):
        psi[i] = kappa_v[i]*M[i] + H * (z[i] - prZ)
        psi_i = psi[i].reshape(k,1)
        psipsi[:,:,i] = np.dot(psi_i,psi_i.T)

    Sigma = psipsi.mean(axis=2)


    J_inv = np.linalg.inv(J)
    omega = np.dot(np.dot(J_inv,Sigma),J_inv)*(1./n)

    std_err_CF =  np.diagonal(omega**.5)


    return betaIVQT, std_err_CF 



# Estimating Sigma


M = np.zeros((n,k))
H = np.zeros((n,k))

for i in range(n):
    W_i = D_cons[i,:].reshape(1,k)
    M[i] = (tau - (u_hat[i] <0)) * W_i 
    H[i] = M[i] * (-d[i]*(1-z[i])/(1-prZ)**2 + -z[i]*(1-d[i])/(prZ)**2)

H = (H.mean(axis = 0))#.reshape((k,1))

psi = np.zeros((n,k))
psipsi = np.zeros((k,k,n))
for i in range(n):
    psi[i] = kappaplus[i]*M[i] + H * (z[i] - prZ)
    psi_i = psi[i].reshape(k,1)
    psipsi[:,:,i] = np.dot(psi_i,psi_i.T)

Sigma = psipsi.mean(axis=2)

print Sigma


